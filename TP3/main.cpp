#include "../lib/include/matrix.hpp"

#include "../lib/include/huffman.hpp"

int main(int argc, char* argv[]) {
	std::string pathname = "../img/parrot.ppm";
	double q = 7.0;
	size_t n = 1;

	if (argc > 1) {
		pathname = std::string(argv[1]);
		if (argc > 2) {
			n = atoi(argv[2]);
			if (argc > 3) {
				q = atof(argv[3]);
			}
		}
	}

	Image i(pathname.c_str());
	i.writeToFile("original_haar.ppm");
	Image j(i);
	i.compress_with_wavelets(n,q);
	i.writeToFile("final_haar.ppm");
	std::cout << "PSNR = " << i.PSNR(j) << std::endl;

	return EXIT_SUCCESS;
}
