#include "./model.hpp"

/* REMINDER :
** Eigen's matrices are accessed in this order :
** 		double m = matrix(row, col);
** To get a matrix rows/cols you can use :
** 		uint32 r = matrix.rows();
** 		uint32 c = matrix.cols(); */

// Model constructor {{{
Model::Model(const std::string& pathName) {
	if (!igl::read_triangle_mesh(pathName, this->vertices, this->faces)){
		std::cerr << "Couldn't read model" << std::endl;
		exit(EXIT_FAILURE);
	}
	this->nbVertices = this->vertices.rows();
	Eigen::Vector3d tempBary = Eigen::Vector3d(.0,.0,.0);
	for (unsigned int i = 0; i < this->vertices.rows(); ++i) {
		tempBary += Eigen::Vector3d(this->vertices(i,0), this->vertices(i,1), this->vertices(i,2));
	}
	tempBary /= (double)this->vertices.rows();
	this->barycenter = Eigen::Vector3d(tempBary);
	this->verticesCoords = coordType::CARTHESIAN;
}
// }}}

// Model draw routine, sending data to the viewer {{{
void Model::draw(igl::opengl::glfw::Viewer & viewer)const {
	viewer.data().set_mesh(this->vertices, this->faces);
	viewer.data().set_face_based(true);
}
// }}}

// Writes the model onto disk {{{
void Model::write(const std::string& pathName) const {
	bool written = igl::write_triangle_mesh(pathName, this->vertices, this->faces);
	return;
}
// }}}

// Populate bins with the correct vertices {{{
std::vector<Model::bin> Model::populateBins(size_t nbBins) {
	// create bins
	std::vector<bin> bins;
	for (unsigned int i = 0; i < nbBins; ++i) { bins.push_back(bin(*this)); }

	// create a vector for bin lower threshold(s)
	std::vector<double> minBins;
	minBins.resize(nbBins);

	// Compute the width of each bin :
	size_t binWidth = (size_t)( std::floor( (double)this->vertices.rows() / (double) nbBins ) );
	// if there are any left, we need to know about it
	size_t left = this->vertices.rows() - nbBins * binWidth;
	// last "full" bin size
	size_t last = this->vertices.rows() - (nbBins-1) * binWidth;

	if (left > 0) {
		// we need to divide it into the proper number of bins, not one more than needed
		binWidth = (size_t)( std::floor( (double)this->vertices.rows() / (double) (nbBins - 1) ) );
		last = this->vertices.rows() - (nbBins-1)*binWidth;
	}

	#ifdef _TP2_DEBUG_
		std::cout << nbBins << " of " << binWidth << " elements with " << last << " elements in the last one." << std::endl;
	#endif

	double rhoMin = .0, rhoMax = .0;
	// compute min/max of model stored into vars with std::tie
	std::tie(rhoMin, rhoMax) = this->computeBoundaries();
	#ifdef _TP2_DEBUG_
		std::cout << "Ω min : " << rhoMin << " | Ω max : " << rhoMax << std::endl;
	#endif

	double alpha = (rhoMax - rhoMin) / (double)nbBins;

	// compute min values possible for each bin :
	for (size_t i = 0; i < nbBins; ++i) {
		minBins[i] = rhoMin + alpha * i;
	}

	// create a secondary vector with position and rho distance, sorted up by rho distance :
	std::vector<std::pair<size_t,double>> indexedRho;
	for (unsigned int i = 0; i < this->vertices.rows(); ++i) {
		indexedRho.push_back(std::pair<size_t, double>(i,this->vertices(i,0)));
	}
	std::sort(indexedRho.begin(),indexedRho.end(),[](const std::pair<size_t,double>&lhs,const std::pair<size_t,double>&rhs)->bool{return lhs.second < rhs.second;});

	size_t curBin = 0;
	// store each vertex into bins :
	for (size_t i = 0; i < indexedRho.size(); ++i) {
		while (curBin != nbBins-1 && indexedRho[i].second > minBins[curBin+1]) {
			curBin++;
		}
		bins[curBin].data.push_back(indexedRho[i].first);
	}

	#ifndef _TP2_DEBUG_
		std::cout << "Bins count : " << std::endl;
		for (unsigned int i = 0; i < bins.size(); ++i) {
			std::cout << "\tBin " << i << " has " << bins[i].data.size() << " elements." << std::endl;
		}
	#endif

	return bins;
}
// }}}

// Compute min/max boundaries for the model, in radial coords {{{
std::pair<double,double> Model::computeBoundaries() const {
	if (this->verticesCoords != coordType::RADIAL) {
		std::cerr << "Couldn't compute min/max with non-radial coordinates" << std::endl;
		return std::pair<double,double>(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity());
	}
	double mi = std::numeric_limits<double>::max();
	double ma = std::numeric_limits<double>::min();
	for (size_t i = 0; i < this->vertices.rows(); ++i) {
		if (this->vertices(i,0) < mi) {
			mi = this->vertices(i,0);
		}
		if (this->vertices(i,0) > ma) {
			ma = this->vertices(i,0);
		}
	}
	return std::pair<double,double>(mi,ma);
}
// }}}

// Normalise coords with bounds {{{
void Model::normaliseWithBoundaries(const double min, const double max) {
	for (unsigned int i = 0; i < this->nbVertices; ++i) {
		double oldVal = this->vertices(i,0);
		this->vertices(i,0) = ( oldVal - min ) / (max-min);
	}
	return;
}
// }}}

// Un-normalise coords with bounds {{{
void Model::unnormaliseWithBoundaries(const double min, const double max) {
	for (unsigned int i = 0; i < this->nbVertices; ++i) {
		double oldVal = this->vertices(i,0);
		this->vertices(i,0) = ( oldVal * (max-min) ) + min;
	}
	return;
}
// }}}

// Insert a message {{{
void Model::insertMessage(const std::string &message, double alpha = 0.25) {
	// First of all, compute the bitstream corresponding to
	// the message string in binary :
	std::vector<bool> bitMessage = str2Bs(message);
	this->switchCoordType(coordType::RADIAL);
	// Then, create as many bins as necessary :
	std::vector<bin> bins = this->populateBins(bitMessage.size());
	// For each bit, we need to incorporate it into the bin :
	for (size_t i = 0; i < bitMessage.size(); ++i) {
		std::cerr << "Bin " << i << " done ...";
		bins[i].insertBit(bitMessage[i], alpha);
	}
	// switch back to carthesian
	this->switchCoordType(coordType::CARTHESIAN);
}
// }}}

// Try to extract a message {{{
std::string Model::extractMessage(const double alpha, size_t nbBins) {
	// convert to radial
	this->switchCoordType(coordType::RADIAL);
	// create bins :
	std::vector<bin> bins = this->populateBins(nbBins);
	// guess a message
	std::vector<bool> guessedMessage;
	guessedMessage.resize(bins.size());
	for (unsigned int i = 0; i < nbBins; ++i) {
		guessedMessage[i] = bins[i].extractBit(alpha);
	}
	// convert to carthesian coordinates
	this->switchCoordType(coordType::CARTHESIAN);
	return bs2Str(guessedMessage);
}
// }}}

// Easy coord mode switch {{{
void Model::switchCoordType(coordType newCoordType) {
	if (newCoordType == this->verticesCoords) {
		std::cout << "Tried to convert to coordinate model already in use !" << std::endl;
		if (this->verticesCoords == coordType::CARTHESIAN) {
			std::cout << "(already in absolute coords)" << std::endl;
		} else {
			std::cout << "(already in radial coords)" << std::endl;
		}
		return;
	}

	if (this->verticesCoords == coordType::CARTHESIAN && newCoordType == coordType::RADIAL) {
		this->switchFromCarthesianToRadial();
		return;
	}

	if (this->verticesCoords == coordType::RADIAL && newCoordType == coordType::CARTHESIAN) {
		this->switchFromRadialToCarthesian();
		return;
	}
}
// }}}

// Carthesian --> Radial switch {{{
void Model::switchFromCarthesianToRadial() {
	this->verticesCoords = coordType::RADIAL;
	double bx = this->barycenter[0], by = this->barycenter[1], bz = this->barycenter[2];
	for (unsigned int i = 0; i < this->nbVertices ; ++i) {
		double ro = 0.0, theta = 0.0, phi = 0.0;
		double x = .0, y = .0, z = .0;
		x = this->vertices(i,0);
		y = this->vertices(i,1);
		z = this->vertices(i,2);
		// compute ro (distance from barycenter)
		ro = std::sqrt( std::pow((x-bx),2.0) + std::pow(y-by,2.0) + std::pow(z-bz,2.0) );
		// compute theta (angle from barycenter on plane XY)
		if (x == bx) {
			theta = 0.0;
		} else {
			theta = atan2((y-by),(x-bx));
		}
		// compute phi (angle from barycenter on plane XZ)
		if (ro != 0.0 && ro > 0.0) {
			phi = acos((z-bz)/ro);
		}
		this->vertices(i,0) = ro;
		this->vertices(i,1) = theta;
		this->vertices(i,2) = phi;
	}
}
// }}}

// Radial --> Carthesian switch {{{
void Model::switchFromRadialToCarthesian() {
	this->verticesCoords = coordType::CARTHESIAN;
	double bx = this->barycenter[0], by = this->barycenter[1], bz = this->barycenter[2];
	for (unsigned int i = 0; i < this->nbVertices ; ++i) {
		double ro = 0.0, theta = 0.0, phi = 0.0;
		double x = .0, y = .0, z = .0;
		ro = this->vertices(i,0);
		theta = this->vertices(i,1);
		phi = this->vertices(i,2);
		// compute x
		x = ro * cos(theta) * sin(phi) + bx;
		// compute theta (angle from barycenter on plane XY)
		y = ro * sin(theta) * sin(phi) + by;
		// compute phi (angle from barycenter on plane XZ)
		z = ro * cos(phi) + bz;
		this->vertices(i,0) = x;
		this->vertices(i,1) = y;
		this->vertices(i,2) = z;
	}
}
// }}}

// str --> bool {{{
std::vector<bool> str2Bs(const std::string& message) {
	std::vector<bool> bitStream;
	for(const char c : message) {
		std::bitset<8> bitRepresentation = std::bitset<8>(c);
		for (unsigned int i = 0; i < 8; ++i) {
			bitStream.push_back(bitRepresentation[i]);
		}
	}
	return bitStream;
}
// }}}

// bool --> str {{{
std::string bs2Str(const std::vector<bool>& message) {
	std::string guessedMessage = "";
	guessedMessage.resize((size_t)(message.size()/8));
	std::cout << "mess : ";
	for (bool b : message) {
		std::cout << b;
	}
	std::cout << std::endl;
	for (unsigned int i = 0; i < message.size(); i+=8) {
		char gc = (message[i+0] << 7) + (message[i+1] << 6) + (message[i+2] << 5) + (message[i+3] << 4) +
			(message[i+4] << 3) + (message[i+5] << 2) + (message[i+6] << 1) + (message[i+7]);
		std::cerr << "guessed char : " << gc << " // " << +gc << std::endl;
		guessedMessage[i/8]=gc;
	}
	return guessedMessage;
}
// }}}

// Bin class and functions {{{

// Bin constructor {{{
Model::bin::bin(Model& model) : parent(model) {
	this->recomputeBarycenter();
}
// }}}

// Recomputation of the barycenter {{{
void Model::bin::recomputeBarycenter() {
	this->barycenter = Eigen::Vector3d(.0,.0,.0);
	for (size_t i = 0; i < this->data.size(); ++i) {
		this->barycenter += Eigen::Vector3d(parent.vertices(i,0),parent.vertices(i,1),parent.vertices(i,2));
	}
	this->barycenter /= (double) this->data.size();
}
// }}}

// Recomputation of min and max values of the bin {{{
void Model::bin::recomputeBoundaries() const {
	if (parent.verticesCoords != coordType::RADIAL) {
		std::cerr << "Couldn't compute the min/max of the bin using carthesian coordinates" << std::endl;
		return;
	}
	for (size_t i = 0; i < this->data.size(); ++i) {
		double curVal = parent.vertices(this->data[i],0);
		if (this->minVal > curVal) {
			this->minVal = curVal;
		}
		if (this->maxVal < curVal) {
			this->maxVal = curVal;
		}
	}
}
// }}}

// Normalisation of values from the bin directly {{{
void Model::bin::normaliseWithBoundaries() {
	for (unsigned int i = 0; i < this->data.size(); ++i) {
		double curVal = this->parent.vertices(this->data[i],0);
		this->parent.vertices(this->data[i],0) = (curVal - this->minVal) / (this->maxVal - this->minVal);
	}
}
// }}}

// Un-normalisation of values from the bin directly {{{
void Model::bin::unNormaliseWithBoundaries() {
	for (unsigned int i = 0; i < this->data.size(); ++i) {
		double curVal = this->parent.vertices(this->data[i],0);
		this->parent.vertices(this->data[i],0) = (curVal * (this->maxVal - this->minVal)) + this->minVal;
	}
}
// }}}

// compute mean value of the bin {{{
double Model::bin::computeMean() const {
	double mean = .0;
	for (size_t idx : this->data) {
		mean += this->parent.vertices(idx,0);
	}
	return mean / (double) this->data.size();
}
// }}}

// Bit insertion for the current bin {{{
void Model::bin::insertBit(bool message, double alpha) {
	// start by recomputing boundaries
	this->recomputeBoundaries();
	// then, normalise all coordinates using
	// the min/max values of the bin
	this->normaliseWithBoundaries();
	// here, we'll assume the sign of ω to be +1 if the bit to
	// insert is 1, and ω = -1 if the bit to insert is 0
	double mean = this->computeMean();
	double k = 1.0;
	size_t nbIt = 0;
	if (message == true) {
		do {
			++nbIt;
			this->powToAll(k);
			mean = this->computeMean();
			k -= _DELTA_K_;
		} while (mean < .5 + alpha);
	} else {
		do {
			++nbIt;
			this->powToAll(k);
			mean = this->computeMean();
			k += _DELTA_K_;
		} while (mean > .5 - alpha);
	}
	std::cerr << " in " << nbIt << " iterations." << std::endl;
	// un-normalise with boundaries
	this->unNormaliseWithBoundaries();
	return;
}
// }}}

// Bit extraction for the current bin {{{
bool Model::bin::extractBit(double alpha) {
	assert(this->parent.verticesCoords == coordType::RADIAL);
	this->normaliseWithBoundaries();
	double mean = this->computeMean();
	this->unNormaliseWithBoundaries();
	return mean > 0.5;
}
// }}}

// Apply to all {{{
void Model::bin::powToAll(double k) {
	for (size_t idx : this->data) {
		this->parent.vertices(idx, 0) = std::pow(this->parent.vertices(idx, 0), k);
	}
}
// }}}

// }}}

