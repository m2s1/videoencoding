#ifndef _TP_MODEL_HPP_
#define _TP_MODEL_HPP_

#include <bitset>
#include <Eigen/Core>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/read_triangle_mesh.h>
#include <igl/write_triangle_mesh.h>
#include <limits>
#include <cmath>

#define _TP2_DEBUG_
#define _DELTA_K_ .0005

typedef enum {
	RADIAL,
	CARTHESIAN
} coordType;

class Model {
	private:
		class bin; // foward declaration for populateBins()
	public:
		/* Constructor from a file on the system */
		Model(const std::string& pathName);
		/* Draw the model using a igl::opengl::glfw::viewer object */
		void draw(igl::opengl::glfw::Viewer&) const;
		/* Write the model on a file on the system */
		void write(const std::string& pathName) const;
		/* Creates bins, and assigns each point to a bin */
		std::vector<bin> populateBins(size_t);
		/* switch coordinates type from one type to another */
		void switchCoordType(coordType);
		/* Returns a pair of the min/max rho values of the whole model */
		std::pair<double,double> computeBoundaries() const;
		/* Normalise vertices using min and max coordinates */
		void normaliseWithBoundaries(const double, const double);
		/* Un-normalise vertices using min and max coordinates */
		void unnormaliseWithBoundaries(const double, const double);
		/* Insert hidden message in the model, using Cho's algorithm */
		void insertMessage(const std::string& message, double);
		// side note, the above function has default params for the last two floating point numbers
		/* Extract message from the model, using a reference alpha */
		std::string extractMessage(const double, size_t);
	protected:
		Eigen::MatrixXd vertices;		/* vertices for the model */
		Eigen::MatrixXi faces;			/* faces for the model */
		Eigen::Vector3d barycenter;		/* barycenter, for later modifications */
		coordType verticesCoords;		/* current coordinate model */
		unsigned int nbVertices;		/* Number of vertices */
	private:
		void switchFromRadialToCarthesian();	/* Switches from radial to absolute coordinates */
		void switchFromCarthesianToRadial();	/* Switches from absolute to radial coordinates */

		// Class for bins in the histogram {{{
		class bin {
			friend class Model;
			public:
				bin(Model& m);				/* Constructor for a bin */
				void recomputeBarycenter();		/* Computes barycenter for the current bin */
				void recomputeBoundaries()const;	/* Recomputes min/max values for the bin */
				double computeMean() const;		/* compute to all */
				void normaliseWithBoundaries();		/* same as model, but relative to bin */
				void unNormaliseWithBoundaries();	/* same as model, but relative to bin */
				void insertBit(bool, double);		/* insert a bit onto the current bin */
				bool extractBit(double);		/* insert a bit onto the current bin */
				void powToAll(double);			/* apply a power of `k` to all data points */
			protected:
				Model& parent;			/* Link to parent to access vertices */
				std::vector<size_t> data;	/* Link to every point inside the bin (in parent model) */
				mutable double minVal;		/* Minimum value of the bin (using radial coordinates) */
				mutable double maxVal;		/* Maximum value of the bin (using radial coordinates) */
				Eigen::Vector3d barycenter;	/* Barycenter of the bin (Useful ???) */
		};
		// }}}

};

std::vector<bool> str2Bs(const std::string& message);
std::string bs2Str(const std::vector<bool>& message);

#endif // _TP_MODEL_HPP_
