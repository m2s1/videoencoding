#include <igl/opengl/glfw/Viewer.h>
#include "./model.hpp"

int main(int argc, char *argv[])
{
	// Inline mesh of a cube
	const Eigen::MatrixXd V= (Eigen::MatrixXd(8,3)<<
		0.0,0.0,0.0,
		0.0,0.0,1.0,
		0.0,1.0,0.0,
		0.0,1.0,1.0,
		1.0,0.0,0.0,
		1.0,0.0,1.0,
		1.0,1.0,0.0,
		1.0,1.0,1.0).finished();
	const Eigen::MatrixXi F = (Eigen::MatrixXi(12,3)<<
		1,7,5,
		1,3,7,
		1,4,3,
		1,2,4,
		3,8,7,
		3,4,8,
		5,7,8,
		5,8,6,
		1,5,6,
		1,6,2,
		2,6,8,
		2,8,4).finished().array()-1;

	// Plot the mesh
	igl::opengl::glfw::Viewer viewer;

	Model test_model("../models/bunny.off");
	test_model.draw(viewer);

	char c = 0x00;
	std::string message = std::string(1,c);
	double alpha = .31;

	test_model.write("before_stegano.off");

	test_model.insertMessage(message, alpha);

	std::string guess = test_model.extractMessage(alpha, message.size()*8);
	std::cout << "message was : " << guess << std::endl;

	test_model.write("after_stegano.off");
	viewer.launch(true, false, "OpenGL : libigl viewer");
}
