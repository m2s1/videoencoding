#include "../../lib/include/image.hpp"

int main(int argc, char* argv[]) {
	Image i("../../img/Airplane.pgm");
	unsigned int p = 11;
	unsigned int q = 23;
	unsigned int e = 17;
	
	std::cout << "Entropy of the original  image : " << i.compute_entropy() << std::endl;
	
	i.writeHistogramTo("histo_original.gp");

	i = i.threshold(180);
	i.writeToFile("original.pgm");

	i.very_simple_rsa(p, q, e);
	i.writeToFile("encrypted_rsa.pgm");
	i.writeHistogramTo("histo_encrypted.gp");
	
	std::cout << "Entropy of the encrypted image : " << i.compute_entropy() << std::endl;
	
	i.very_simple_reverse_rsa((p-1)*(q-1), p*q, e);
	i.writeToFile("decrypted_rsa.pgm");
	i.writeHistogramTo("histo_decrypted.gp");
	
	std::cout << "Entropy of the decrypted image : " << i.compute_entropy() << std::endl;
	return EXIT_SUCCESS;
}
