#include "../../lib/include/image.hpp"

int main(int argc, char* argv[]) {
	Image i("../../img/10.pgm");
	i.encrypt(56);
	i.writeToFile("encrypted.pgm");
	i.decrypt_brute_force();
	i.writeToFile("decrypted_brute_force.pgm");
	i.compute_entropy();
	return EXIT_SUCCESS;
}
