#include "../lib/include/image.hpp"

int main(int argc, char* argv[]) {
	// why must I do this
	Image baseImage = Image("../img/parrot.ppm");
	Image originalCopy = Image(baseImage);
	baseImage.squeezeAndExpandInYCrCb();
	double psnr = baseImage.PSNR(originalCopy);
	baseImage.writeToFile("./squeezedImage.ppm");
	std::cout << "PSNR is : " << psnr << std::endl;
	return EXIT_SUCCESS;
}
