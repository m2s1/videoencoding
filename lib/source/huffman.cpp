#include "../include/huffman.hpp"

#include <map>
#include <queue>
#include <algorithm>

void tree_node::print_node(std::string indent) const {
	std::cout << indent << "tree_node (depth = " << indent.size() << ") : freq at " << this->f << std::endl;
	this->left->print_node(indent + " ");
	this->right->print_node(indent + " ");
}

template <typename data_class> void data_node<data_class>::print_node(std::string indent) const {
	std::cout << indent << "data_node data : " << +this->data << " // freq = " << this->f << std::endl;
}

image_huffman_generator::image_huffman_generator(Image& i, size_t plane) : ref_image(i), tree_root(nullptr) {
	// Copy all data in an image effectively nb_planes times wider
	// than the original, but with all channels in one Huffman tree
	if (plane > i.getSize() / i.getPixelCount()) {
		this->image_data.resize(i.getHeight());
		size_t nb_planes = i.getSize() / i.getPixelCount();
		size_t height = i.getHeight();
		size_t width = i.getWidth();
		for (size_t j = 0; j < height; ++j) {
			for (size_t k = 0; k < nb_planes; ++k) {
				for (size_t l = 0; l < i[k][j].size(); ++l) {
					this->image_data[j].push_back(i[k][j][l]);
				}
			}
		}
	} else {
		this->image_data = i[plane];
	}
}

size_t image_huffman_generator::compute_huffman_tree_generation() {
	// first, get all known chars in an std::map for counting purposes
	std::vector<freq_t> frequencies;
	frequencies.resize(256);

	for (const std::vector<unsigned char>& row : this->image_data) {
		for (const unsigned char& pixel : row) {
			frequencies[pixel]++;
		}
	}
	// we now got a map of all known chars and their occurences
	// build a sorted queue for counting all nodes in order
	std::priority_queue<generic_node*, std::vector<generic_node*>, node_comparator> sorted_list;

	for (size_t i = 0; i < frequencies.size(); ++i) {
		const freq_t freq = frequencies[i];
		if (freq > 0) {
			sorted_list.push(new data_node<unsigned char>(freq, i));
		}
	}

	// we just sorted all symbols by occurences, and created leaf nodes for all of them
	while (sorted_list.size() > 1) {
		// get a node and add it to its closest neighbor
		generic_node* most_freq = sorted_list.top();
		sorted_list.pop();

		generic_node* less_freq = sorted_list.top();
		sorted_list.pop();

		generic_node* parent = new tree_node(most_freq, less_freq);
		sorted_list.push(parent);
	}
	this->tree_root = sorted_list.top();

	// the tree is now created.
	// we need to get a map of how long all the characters are
	std::map<unsigned char, size_t> symbol_sizes;
	std::stack<bool> tree_traversal_bitstream;
	image_huffman_tree_traversal(this->tree_root, tree_traversal_bitstream, symbol_sizes);

	size_t mean = 0;
	// traverse the map, get frequencies of each symbol
	// and add them multiplied by the Huffman symbol associated
	for (const auto& sizes : symbol_sizes) {
		// if it does indeed appear in the message :
		mean += frequencies[sizes.first] * sizes.second;
	}
	return mean;
}

void image_huffman_tree_traversal(const generic_node* current_node, std::stack<bool>& current_tree_traversal_bitstream, std::map<unsigned char, size_t>& symbol_sizes) {
	// first of all, the condition to stop recursion :
	if (current_node->get_node_type() == huffman_tree_data_types::DATA) {
		const data_node<unsigned char>* data = dynamic_cast<data_node<unsigned char>*>(const_cast<generic_node*>(current_node));
		assert(symbol_sizes.count(data->data) == 0);
		symbol_sizes.insert(std::make_pair(data->data, current_tree_traversal_bitstream.size()));
		return;
	}

	if (current_node->get_node_type() == huffman_tree_data_types::GENERIC) {
		std::cerr << "================== JE SUIS PAS CENSE ETRE LA : generic node taken as input for tree traversal =================" << std::endl;
	}

	const tree_node* current_parent = dynamic_cast<tree_node*>(const_cast<generic_node*>(current_node));

	current_tree_traversal_bitstream.push(false);
	image_huffman_tree_traversal(current_parent->right, current_tree_traversal_bitstream, symbol_sizes);
	current_tree_traversal_bitstream.pop();

	current_tree_traversal_bitstream.push(true);
	image_huffman_tree_traversal(current_parent->left, current_tree_traversal_bitstream, symbol_sizes);
	current_tree_traversal_bitstream.pop();

	return;
}
