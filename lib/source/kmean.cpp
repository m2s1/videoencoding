#include "../include/kmean.hpp"

#include <time.h>
#include <limits>
#include <iomanip>
#include <ios>

// Constructor providing all arguments to clusterer. {{{
imageKMeanClusterer::imageKMeanClusterer(const std::vector<std::vector<std::vector<unsigned char>>> imgData,
						unsigned int nbClusters, unsigned int nbIter, double delta) {
	bool isColor = (imgData.size() > 1);

	unsigned char r = 0, g = 0, b = 0;

	std::size_t imgHeight = imgData[0].size();
	std::size_t imgWidth = imgData[0][0].size();
	std::size_t imgSize = imgHeight * imgWidth;

	// Set a new random seed
	srand(time(NULL));

	this->maxIter = nbIter;
	this->minDelta = delta;
	this->clusterCenters.resize(nbClusters);
	this->rawData.resize(0);
	this->labels.resize(0);
	this->height = imgHeight;
	this->width = imgWidth;

	unsigned int errorAssignation = 0;

	// Assign random cluster centers in RGB space {{{
	for (unsigned int clustIt = 0; clustIt < nbClusters; ++clustIt) {
		/* Original assignation : random in RGB space {{{
		r = (unsigned char) (rand() % 255);
		// If the image is color, assign new value
		// for G and B, else take the same
		if (isColor) {
			g = (unsigned char) (rand() % 255);
			b = (unsigned char) (rand() % 255);
		} else {
			b = g = r;
		}
		this->clusterCenters[clustIt] = rgb(r,g,b);
		}}} */

		unsigned int h = (unsigned int) (rand()%imgData[0].size());
		unsigned int w = (unsigned int) (rand()%imgData[0][0].size());

		r = imgData[0][h][w];
		g = imgData[1][h][w];
		b = imgData[2][h][w];

		this->clusterCenters[clustIt] = rgb(r,g,b);

		r = 0;
		g = 0;
		b = 0;
	}
	// }}}

	// Assign image data to rawData AND nearest cluster center {{{
	for (unsigned int yIt = 0; yIt < imgHeight; ++yIt) {
		for (unsigned int xIt = 0; xIt < imgWidth; ++xIt) {
			// Grab color of the current pixel
			if (isColor) {
				r = (double)imgData[0][yIt][xIt];
				g = (double)imgData[1][yIt][xIt];
				b = (double)imgData[2][yIt][xIt];
			} else {
				r = g = b = (double)imgData[0][yIt][xIt];
			}
			rgb currentColor = rgb(r,g,b);
			r = g = b = 0;

			// computes the nearest cluster center
			double minDist = std::numeric_limits<double>::max();
			unsigned int label = nbClusters+1;
			for (unsigned int clustIt = 0; clustIt < nbClusters; ++clustIt) {
				if ( (this->clusterCenters[clustIt] - currentColor).length() < minDist ) {
					minDist = (this->clusterCenters[clustIt] - currentColor).length();
					label = clustIt;
				}
			}
			if (label == nbClusters+1) {
				fprintf(stderr, "WARNING : data point doesn't have a cluster center ! Could segfault !\n");
				errorAssignation++;
			}

			// Since the vectors are filled at the same time, we can
			// ensure each data point has a correct label attached
			this->rawData.push_back(currentColor);
			this->labels.push_back(label);
		}
	}
	// }}}

	if (errorAssignation > 0) {
		fprintf(stderr, "ERROR : Data points not assigned, exiting.\n");
		exit(EXIT_FAILURE);
	}

}
// }}}

// Computes the clusters contained in this image using kMeans. {{{
std::vector<std::vector<std::vector<unsigned char>>> imageKMeanClusterer::compute(unsigned int paletteWidth) {
	// We start with up-to-date labels on each  data point, and
	// a few cluster centers randomly distributed through RGB
	// or greyscale space.

	unsigned int currentIteration = 0;
	double peakIterationDelta = 0.0;
	std::vector<rgb> backupClusters = std::vector<rgb>(this->clusterCenters); // does a deep copy

	this->printPalette("./colorPaletteOriginal.ppm", paletteWidth);

	this->printClusterInfo();

	// We'll start the loop here :
	do {

		// Reset delta for next iteration
		peakIterationDelta = 0.0;

		std::cout << "Iteration " << currentIteration  << " ...";

		this->updateClusterCenters();
		this->updateLabels();


		// Delta computation {{{
		for (unsigned int i = 0; i < this->clusterCenters.size(); ++i) {
			rgb norm1 = this->clusterCenters[i].normalize();
			rgb norm2 = backupClusters[i].normalize();
			rgb norm  = norm1 - norm2;
			double thisDelta = norm.length();
			if (!isnan(thisDelta)) {
				peakIterationDelta = std::max(thisDelta, peakIterationDelta);
			}
		}
		// Take current cluster centers as new backups
		backupClusters.clear();
		// backupClusters.assign(this->clusterCenters.begin(), this->clusterCenters.end());
		backupClusters = this->clusterCenters;
		// }}}

		std::cout << " done with a peak delta of " << std::fixed << peakIterationDelta << std::endl;

		currentIteration++;

	} while (currentIteration < this->maxIter && peakIterationDelta > this->minDelta);

	this->printPalette("./colorPaletteFinsihed.ppm", paletteWidth);

/*	for (unsigned int i = 0; i < this->clusterCenters.size(); ++i) {
		// print into greyscale : quick hack
		// TODO : make this not a hack
		double place = (double)i;
		this->clusterCenters[i] = rgb(place,place,place);
	}
	printf("converted to greyscale\n");*/

	this->printClusterInfo();

	// Conversion to image matrix {{{
	std::vector<std::vector<std::vector<unsigned char>>> result;
	result.resize(3);
	for (unsigned int i = 0; i < 3; ++i) {
		result[i].resize(this->height);
		for (unsigned int j = 0; j < this->height; ++j) {
			result[i][j].resize(width);
		}
	}

	for (unsigned int i = 0; i < this->height; ++i) {
		for (unsigned int j = 0; j < this->width; ++j) {
			unsigned int assignedCluster = this->labels[i*this->width + j];
			result[0][i][j] = this->clusterCenters[assignedCluster].r;
			result[1][i][j] = this->clusterCenters[assignedCluster].g;
			result[2][i][j] = this->clusterCenters[assignedCluster].b;
		}
	}
	// }}}

	return result;
}
// }}}

// Computes the new cluster center positions using label data. {{{
void imageKMeanClusterer::updateClusterCenters() {
	// init sum at zero for all cluster centers
	std::vector<rgb> clusterSum = std::vector<rgb>();
	clusterSum.resize(this->clusterCenters.size());
	std::vector<unsigned int> clusterSize = std::vector<unsigned int>();
	clusterSize.resize(this->clusterCenters.size());
	for (unsigned int i = 0; i < this->clusterCenters.size(); ++i) {
		clusterSum[i] = rgb(0.0,0.0,0.0);
		clusterSize[i] = 0;
	}

	// iterate on data and sum using labels
	for (unsigned int i = 0; i < this->labels.size(); ++i) {
		clusterSum[this->labels[i]] += this->rawData[i];
		clusterSize[this->labels[i]]++;
	}

	// update cluster center coordinates
	for (unsigned int i = 0; i < this->clusterCenters.size(); ++i) {
		if (clusterSize[i] != 0) {
			this->clusterCenters[i] = clusterSum[i] / (double)clusterSize[i];
		}
	}

	return;
}
// }}}

// Computes new labels for each data point in the dataset. {{{
void imageKMeanClusterer::updateLabels() {
	unsigned int errorAssignation = 0;
	for (unsigned int i = 0; i < this->rawData.size(); ++i) {
		double minDist = std::numeric_limits<double>::max();
		unsigned int newLabel = this->clusterCenters.size()+1;
		for (unsigned int c = 0; c < this->clusterCenters.size(); ++c) {
			if ((this->clusterCenters[c] - this->rawData[i]).length() < minDist) {
				minDist = (this->clusterCenters[c] - this->rawData[i]).length();
				newLabel = c;
			}
		}
		if (newLabel == this->clusterCenters.size() +1) {
			errorAssignation++;
			fprintf(stderr, "WARNING : data point doesn't have a cluster center ! Could segfault !\n");
		}
		this->labels[i] = newLabel;
	}

	if (errorAssignation > 0) {
		fprintf(stderr, "ERROR : Data points not assigned, exiting.\n");
		exit(EXIT_FAILURE);
	}

	return;
}
// }}}

// Prints the color palette of the cluster centers to disk. {{{
void imageKMeanClusterer::printPalette(const char *pathName, unsigned int _r) const {
	unsigned int nbRows = _r;
	unsigned int nbCols = this->clusterCenters.size() / nbRows + ((this->clusterCenters.size() % nbRows != 0) ? 1 : 0);
	unsigned int pixelsPerColor = 32;
	unsigned int imgWidth = nbRows * pixelsPerColor;
	unsigned int imgHeight = nbCols * pixelsPerColor;

	FILE* imgFile = fopen(pathName, "wb");
	if (imgFile == NULL) {
		fprintf(stderr, "imageKMeanClusterer::printPalette() : Couldn't open file %s. Stopping.\n", pathName);
		return;
	}

	fprintf(imgFile, "P6\n");
	fprintf(imgFile, "# imgSize : %d x %d = %d for %d rows %d cols\n", imgWidth, imgHeight, imgWidth*imgHeight, nbRows, nbCols);
	fprintf(imgFile, "%d %d\n%d\n", imgWidth, imgHeight, 255);

	unsigned char* rawPalette = new unsigned char[imgHeight*imgWidth*3];

	// Start printing the color palette
	for (unsigned int i = 0; i < imgHeight; ++i) {
		for (unsigned int j = 0; j < imgWidth*3; j+=3) {
			unsigned int curRow = i/pixelsPerColor;
			unsigned int curCol = (j/3)/pixelsPerColor;
			rawPalette[i * imgWidth*3 + j + 0] = (unsigned char)this->clusterCenters[curRow * nbRows + curCol].r;
			rawPalette[i * imgWidth*3 + j + 1] = (unsigned char)this->clusterCenters[curRow * nbRows + curCol].g;
			rawPalette[i * imgWidth*3 + j + 2] = (unsigned char)this->clusterCenters[curRow * nbRows + curCol].b;
		}
	}

	// write to file
	if (fwrite(rawPalette, sizeof(unsigned char), imgWidth*imgHeight*3, imgFile) != imgHeight*imgWidth*3) {
		fprintf(stderr, "imageKMeanClusterer::printPalette() : Error whilst writing to image.\n");
		return;
	}

	#ifdef DEBUG
	fprintf(stderr, "imageKMeanClusterer::printPalette() : Palette written in a %dx%d matrix\n\t to file \"%s\".\n", nbRows, nbCols, pathName);
	#endif
}
// }}}

// Prints info about the clusters {{{
void imageKMeanClusterer::printClusterInfo() const {
	std::cout << "For the " << this->clusterCenters.size() << " clusters :" << std::endl;
	unsigned int allPts = 0;
	for (unsigned int i = 0; i < this->clusterCenters.size(); ++i) {
		unsigned int occurences = 0;
		for (unsigned int j = 0; j < this->labels.size(); ++j) {
			if (this->labels[j] == i) occurences++;
		}

		std::cout << "\tCenter is at " << this->clusterCenters[i] << " and has " << occurences << " points attached to it." << std::endl;
		allPts += occurences;
	}
	if (allPts == this->rawData.size()) {
		std::cout << "All points accounted for" << std::endl;
	} else {
		std::cout << "Problem detected" << std::endl;
	}
}
// }}}
