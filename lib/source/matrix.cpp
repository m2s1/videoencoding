#include "../include/matrix.hpp"

#include <iostream>
#include <algorithm>
#include <cassert>

/*	default error template to do :
	#ifdef __MATRIX_DEBUG_ON
	#endif
*/

// Constructor from an image {{{
matrix::matrix(Image& i, size_t plane) : original_image(i) {
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::matrix() : called" << std::endl;
	std::cerr << "\t\t   Image provided. Size : " << i.getHeight() << "x" << i.getWidth() << " pixels." << std::endl;
	std::cerr << "\t\t   Currently copying plane : " << plane << "." << std::endl;
#endif
	this->raw_data.resize(i.getHeight());
#ifdef __MATRIX_DEBUG_ON
#else
	this->path_name_for_debug = "";
#endif
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "\t\t   Allocated " << this->raw_data.size() << " rows for image." << std::endl;
#endif
	for (size_t k = 0; k < this->raw_data.size(); ++k) {
		std::vector<unsigned char> temp = i[plane][k];
		for (unsigned char c : temp) {
			this->raw_data[k].push_back(static_cast<double>(c));
		}
#ifdef __MATRIX_DEBUG_ON
		std::cerr << "\t\t\t   Allocated " << this->raw_data[k].size() << " cols for row " << k << "." << std::endl;
#endif
	}
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::matrix(const Image&, size_t) : call ended." << std::endl;
	std::cerr << std::endl << std::endl;
#endif
}
// }}}

// Haar transform on the matrix {{{
void matrix::haar_transform() {
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::haar_transform() called" << std::endl;
#endif
	if (this->raw_data.size() % 2 == 1 || this->raw_data[0].size() % 2 == 1) {
		std::cerr << "Couldn't perform haar transform on image of non-conform size." << std::endl;
		return;
	}
	std::string pathname = "";
	pathname = "intermediary_results/fwd_"+path_name_for_debug;
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "\t\t   Outputting intermediate images to " << pathname << "<step>.pgm" << std::endl;
#endif
	return this->haar_transform(1, pathname);
}
// }}}

// Inverse Haar transform on the matrix {{{
void matrix::inverse_haar() {
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::inverse_haar() called" << std::endl;
#endif
	if (this->raw_data.size() == 0  || this->raw_data.size() % 2 == 1 || this->raw_data[0].size() % 2 == 1) {
		std::cerr << "Couldn't perform haar transform on image of non-conform size." << std::endl;
		return;
	}
	std::string pathname;
	pathname = "intermediary_results/inv_"+path_name_for_debug;
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "\t\t   Outputting intermediate images to " << pathname << "<step>.pgm" << std::endl;
#endif
	return this->inverse_haar(this->max_haar_reached, pathname);
}
// }}}

// Copy data to image {{{
void matrix::copy_to_image(Image & img, size_t plane) const {
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::copy_to_image() : called on plane " << plane << "." << std::endl;
#endif
	size_t underflows = 0;
	size_t overflows = 0;
	for (size_t i = 0; i < this->raw_data.size(); ++i) {
		for (size_t j = 0; j < this->raw_data[i].size(); ++j) {
			if (this->raw_data[i][j] > static_cast<double>(std::numeric_limits<unsigned char>::max())) { overflows++; }
			if (this->raw_data[i][j] < 0.0) { underflows++; }
			img[plane][i][j] = static_cast<unsigned char>( std::round(this->raw_data[i][j]) );
		}
	}
#ifdef __MATRIX_DEBUG_ON
	std::string str_plane = "red";
	if(plane == 0) { str_plane = "red/greyscale"; }
	else if (plane == 1) { str_plane = "green"; }
	else { str_plane = "blue"; }
	std::cerr << "Copy to image on plane " << str_plane << " concluded with " << overflows << " overflows and " << underflows << " underflows." << std::endl;
#endif
	return;
}
// }}}

// Apply step N of Haar transform {{{
void matrix::haar_transform(size_t _iteration, const std::string& path_name_debug) {
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::haar_transform() called with params : ";
	std::cerr << "(_iteration = " << +_iteration << ", path_name_debug = " << path_name_debug << ")" << std::endl;
#endif

	// Sanity checks {{{
	if (_iteration > this->max_haar) {
		std::cerr << "Forward Haar transform : reached the end of possible iterations. Returning." << std::endl;
		return;
	}
	size_t curIt = _iteration - 1;
	// full height :
	size_t full_size = this->raw_data.size();
	// current max height :
	size_t current_size = full_size / static_cast<size_t>(std::pow(2.0, static_cast<double>(curIt)));
	// full width :
	size_t max_height = current_size;
	// current max width :
	size_t max_width = this->raw_data[0].size() / static_cast<size_t>(std::pow(2.0, static_cast<double>(curIt)));

	// check the current size is also a multiple of two
	if (full_size % 2 == 1) {
		std::cerr << "Forward Haar transform : Couldn't go farther than step " << _iteration << "." << std::endl;
		return;
	}
	if (max_width % 2 == 1) {
		std::cerr << "Current matrix width : " << max_width << std::endl;
		std::cerr << "Forward Haar transform : Couldn't continue. Wrong matrix (COLS WRONG)." << std::endl;
		return;
	}
	// }}}

	double q = std::pow(2.0, this->quantification + static_cast<double>(_iteration));

	this->max_haar_reached = _iteration;

	// Row-by-row step {{{

	// for each row :
	for (size_t i = 0; i < max_height; ++i) {
		// create a copy of the current vector
		std::vector<double> temp;
		temp.resize(max_width);
		// iterate on the current vector :
		for (size_t j = 0; j < max_width; j+=2) {
			double avg = (this->raw_data[i][j] + this->raw_data[i][j+1]) / 2.0;
			double dif = (this->raw_data[i][j] - this->raw_data[i][j+1]) / 2.0;
			temp[j/2] = avg;
#ifndef __MATRIX_APPLY_QUANTIFICATION
			temp[max_width/2 + j/2] = dif;
#else
			temp[max_width/2 + j/2] = dif/q;
#endif
		}
		std::copy(temp.begin(), temp.end(), this->raw_data[i].begin());
	}

	// }}}

#ifdef __MATRIX_DEBUG_ON
#ifdef __MATRIX_DEBUG_LEVEL_BASIC
	std::cerr << "ITERATION " << _iteration  << " - after RbR" << std::endl;
	std::cerr << *this << std::endl;
#endif
#endif

	// col-by-col step {{{
	for (size_t i = 0; i < max_width; ++i) {
		// create a copy of the current vector
		std::vector<double> temp;
		temp.resize(max_height);
		// iterate on the current vector :
		for (size_t j = 0; j < max_height; j+=2) {
			double avg = (this->raw_data[j][i] + this->raw_data[j+1][i]) / 2.0;
			double dif = (this->raw_data[j][i] - this->raw_data[j+1][i]) / 2.0;
			temp[j/2] = avg;
#ifndef __MATRIX_APPLY_QUANTIFICATION
			temp[max_height/2 + j/2] = dif;
#else
			temp[max_height/2 + j/2] = dif/q;
#endif
		}
		for (size_t j = 0; j < max_height; ++j) {
			this->raw_data[j][i] = temp[j];
		}
	}
	// }}}

#ifdef __MATRIX_DEBUG_ON
#ifdef __MATRIX_DEBUG_LEVEL_BASIC
	std::cerr << "ITERATION " << _iteration << " - after CbC" << std::endl;
	std::cerr << *this << std::endl;
#endif
#endif
	if (!path_name_for_debug.empty()) {
		this->output_current_state_to(path_name_debug+(std::to_string(curIt))+".pgm");
	}
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::haar_transform(" << +_iteration << ", " << path_name_debug << ") : call ended." << std::endl;
	std::cerr << std::endl;
#endif
	return this->haar_transform(_iteration+1, path_name_debug);
}
// }}}

// Apply nth step of inverse haar {{{
void matrix::inverse_haar(size_t _iteration, const std::string& path_name_debug) {
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::inverse_haar() called with params : ";
	std::cerr << "(_iteration = " << +_iteration << ", path_name_debug = " << path_name_debug << ")" << std::endl;
#endif

	double q = std::pow(2.0, this->quantification + static_cast<double>(_iteration));

	if (this->max_haar_reached == 0 || _iteration == 0) {
		std::cerr << "Matrix inversion successfully done." << std::endl;
		return;
	}
	size_t cur_height = this->raw_data.size() / static_cast<size_t>(std::pow(2.0,static_cast<double>(_iteration)));
	size_t max_height = 2 * cur_height;

	size_t cur_width = this->raw_data[0].size() / static_cast<size_t>(std::pow(2.0,static_cast<double>(_iteration)));
	size_t max_width = 2 * cur_width;

	// col by col step :
	for (size_t i = 0; i < max_width; ++i) {
		std::vector<double> temp;
		temp.resize(max_height);
		for (size_t j = 0; j <  cur_height; ++j) {
			double a = this->raw_data[j][i];
#ifdef __MATRIX_APPLY_QUANTIFICATION
			double d = this->raw_data[cur_height+j][i];
#else
			double d = this->raw_data[cur_height+j][i] * q;
#endif
			double v1 = (a+(d));
			double v2 = (a-(d));
			temp[j*2+0] = v1;
			temp[j*2+1] = v2;
		}
		for (size_t j = 0; j < max_height; ++j) {
			this->raw_data[j][i] = temp[j];
		}
	}

#ifdef __MATRIX_DEBUG_ON
#ifdef __MATRIX_DEBUG_LEVEL_BASIC
	std::cerr << "ITERATION " << _iteration  << " - after CbC" << std::endl;
	std::cerr << *this << std::endl;
#endif
#endif

	// row by row step :
	for (size_t i = 0; i <  max_height; ++i) {
		std::vector<double> temp;
		temp.resize(max_width);
		for (size_t j = 0; j <  cur_width; ++j) {
			double a = this->raw_data[i][j];
#ifndef __MATRIX_APPLY_QUANTIFICATION
			double d = this->raw_data[i][cur_width+j];
#else
			double d = this->raw_data[i][cur_width+j] * q;
#endif
			double v1 = (a+(d));
			double v2 = (a-(d));
			temp[j*2+0] = v1;
			temp[j*2+1] = v2;
		}
		std::copy(temp.begin(), temp.end(), this->raw_data[i].begin());
	}

#ifdef __MATRIX_DEBUG_ON
#ifdef __MATRIX_DEBUG_LEVEL_BASIC
	std::cout << "INVERSE : " << _iteration << " - after RbR" << std::endl;
	std::cout << *this << std::endl;
#endif
#endif
	if (!path_name_for_debug.empty()) {
		this->output_current_state_to(path_name_debug+(std::to_string(_iteration))+".pgm");
	}
#ifdef __MATRIX_DEBUG_ON
	std::cerr << "matrix::inverse_haar(" << +_iteration << ", " << path_name_debug << ") : call ended." << std::endl;
	std::cerr << std::endl;
#endif
	return this->inverse_haar(_iteration-1, path_name_debug);
}
// }}}

// Debug {{{
void matrix::output_current_state_to(const std::string& path_name) const {
	Image img (original_image);
	img = img.toGreyscale();
	assert(img.getImageType() == utils::loadedType::GREYSCALE);

	size_t underflows = 0;
	size_t overflows = 0;
	for (size_t i = 0; i < this->raw_data.size(); ++i) {
		for (size_t j = 0; j < this->raw_data[i].size(); ++j) {
			if (this->raw_data[i][j] > static_cast<double>(std::numeric_limits<unsigned char>::max())) {
				overflows++;
			}
			if (this->raw_data[i][j] < 0.0) {
				underflows++;
			}
			img[0][i][j] = static_cast<unsigned char>( std::round(this->raw_data[i][j]) );
		}
	}
	img.writeToFile(path_name.c_str());
}
// }}}
