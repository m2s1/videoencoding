#include "../include/image.hpp"
#include "../include/matrix.hpp"
#include "../include/huffman.hpp"
#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <cmath>
#include <list>
#include <algorithm>

// {{{ Constructors

Image::Image() {
	imageType = utils::loadedType::NONE;
	nbPlanes = 0;
	width = 0;
	height = 0;
}

Image::Image(const char* pathName) {
	this->load(pathName);
}

Image::Image(const Image& i) {
#ifdef DEBUG
	fprintf(stderr, "\n\nImage::Image(const Image&) called.\n");
	fprintf(stderr, "Image::Image(const Image&) : copying attributes.\n");
#endif

	this->nbPlanes = i.nbPlanes;
	this->imageType = i.imageType;
	this->width = i.width;
	this->height = i.height;
	this->maxGreyValue = i.maxGreyValue;

#ifdef DEBUG
	fprintf(stderr, "Image::Image(const Image&) : starting to copy matrix ...\n");
#endif

	this->imageMatrix = i.imageMatrix;

#ifdef DEBUG
	fprintf(stderr, "Image::Image(const Image&) : finished vector copy.\n");
	fprintf(stderr, "Image::Image(const Image&) : call ended.\n");
#endif
}

Image::Image(const Image& i, const std::vector<rgb_uc>& pixels) {
	this->width = i.width;
	this->height = i.height;
	this->nbPlanes = 3;
	this->maxGreyValue = i.maxGreyValue;
	this->imageType = utils::loadedType::COLOR;

	this->imageMatrix.resize(this->nbPlanes);
	for (int j = 0; j < this->nbPlanes; ++j) {
		this->imageMatrix[j].resize(this->height);
		for(int i = 0; i < this->height; ++i) {
			this->imageMatrix[j][i].resize(this->width);
		}
	}

	for (unsigned int k = 0; k < this->height; ++k) {
		for (unsigned int j = 0; j < this->width; ++j) {
			this->imageMatrix[0][k][j] = pixels[k * this->width + j].r;
			this->imageMatrix[1][k][j] = pixels[k * this->width + j].g;
			this->imageMatrix[2][k][j] = pixels[k * this->width + j].b;
		}
	}
}

// }}}

// {{{ Accessors

const unsigned int Image::getWidth() const noexcept {
	return this->width;
}

const unsigned int Image::getHeight() const noexcept {
	return this->height;
}

const unsigned int Image::getPixelCount() const noexcept {
	return this->width * this->height;
}

const unsigned int Image::getSize() const noexcept {
	return this->width * this->height * this->nbPlanes;
}

const utils::loadedType Image::getImageType() const noexcept {
	return this->imageType;
}

// }}}

// Setters {{{

void Image::setSize(size_t h, size_t w, size_t p) {
	this->imageMatrix.resize(p);
	for (size_t i = 0; i < p; ++i) {
		this->imageMatrix[i].resize(h);
		for (size_t j = 0; j < height; ++j) {
			this->imageMatrix[i][j].resize(w);
		}
	}
}

void Image::operator()(size_t p, size_t h, size_t w, unsigned char v) {
	this->imageMatrix[p][h][w] = v;
	return;
}

// }}}

// {{{ I/O


void utils::ignorerCommentaires(FILE* imageFile) {
	unsigned char c;
	while ( (c=fgetc(imageFile)) == '#' )
		while ( (c=fgetc(imageFile)) != '\n' );
			// Do nothing.
	fseek(imageFile, -sizeof(unsigned char), SEEK_CUR);
}

Image& Image::load(const char* pathName) {

#ifdef DEBUG
	fprintf(stderr, "\n\nImage::load() called\n");
#endif

	// Try to open the file :
	FILE* imageFile = fopen(pathName, "r");

	// If the file couldn't be found, exit the program.
	if (imageFile == NULL) {
	printf("Image::load() : The file %s could not be opened.\n", pathName);
	exit(EXIT_FAILURE);
	}

#ifdef DEBUG
	fprintf(stderr, "Image::load() : Starting file read from location %s ...\n", pathName);
#endif

	int imgType;

	fscanf(imageFile, "P%i ", &imgType);
	utils::ignorerCommentaires(imageFile);
	fscanf(imageFile, "%u %u %u%*c", &width, &height, &maxGreyValue);

#ifdef DEBUG
	fprintf(stderr, "Image::load() : Attempting to read an image of size %ix%i\n", width, height);
#endif

	// If the image type is not recognized, just leave it.
	if (imgType < 5 || imgType > 6) {
	printf("Image::load() : The image type could not be recognized.\n");
	printf("\tImage type was read as P%i, which is not known.\n", imgType);
	exit(EXIT_FAILURE);
	}

	if (maxGreyValue > 255) {
	printf("Image::load() : The image has too much color values possible per\n");
	printf("\tpixel. This library supports up to 255 greyscale values,\n");
	printf("\twhile this image contains %i\n", maxGreyValue);
	exit(EXIT_FAILURE);
	}

	unsigned char* rawData;

	// Load the image as a greyscale image :
	if (imgType == 5) {
	imageType = utils::loadedType::GREYSCALE;
	fprintf(stderr, "Image::load() : Loaded type : GREYSCALE\n");
	nbPlanes = 1;
	}
	// Load the image as a color image :
	if (imgType == 6) {
	imageType = utils::loadedType::COLOR;
	fprintf(stderr, "Image::load() : Loaded type : COLOR\n");
	nbPlanes = 3;
	}

#ifdef DEBUG
	fprintf(stderr, "Image::load() : Allocating rawData space ...\n");
#endif

	int imgSize = width * height * nbPlanes;
	rawData = (unsigned char*) malloc(imgSize*sizeof(unsigned char));

#ifdef DEBUG
	fprintf(stderr, "Image::load() : Reading process for raw data started.\n");
#endif

	if (fread(rawData, sizeof(unsigned char), imgSize, imageFile) != (size_t)imgSize) {
		printf("Image::load() : Error whilst reading image.\n");
	#ifdef DEBUG
		fprintf(stderr, "Image::load() : Attempted to read a %i-values image has not been succesful.\n", imgSize);
	#endif
		exit(EXIT_FAILURE);
	}

#ifdef DEBUG
	fprintf(stderr, "Image::load() : Starting to allocate space to vector<vector<vector<unsigned char>>>.\n");
#endif

	// Allocate the matrix needed for storing the image
	imageMatrix.resize(nbPlanes);
	for (unsigned int i = 0; i < nbPlanes; ++i) {
		imageMatrix[i].resize(height);
		for (unsigned int j = 0; j < height; ++j) {
			imageMatrix[i][j].resize(width);
		}
	}

#ifdef DEBUG
	fprintf(stderr, "Image::load() : Finished allocating space for vector<vector<vector<unsigned char>>>\n");
	fprintf(stderr, "Image::load() : Starting writing from rawData to vector<vector<vector<unsigned char>>>.\n");
#endif

	// If the image is a greyscale image, then it can also be a binary
	// image saved as a greyscale one. We have to check for that.
	// If the image is supposed to be a greyscale image, then we
	// assume it to be a binary image until proven otherwise :
	int v1 = (int) rawData[0];
	int v2 = -1;
	bool isBinary = true;

	// Load the values in the vector :
	for (unsigned int y = 0; y < height; ++y) {
		for (unsigned int x = 0; x < width; ++x) {
			if (imageType == utils::loadedType::COLOR) {
				imageMatrix[0][y][x] = rawData[y*3*width + x*3 + 0];
				imageMatrix[1][y][x] = rawData[y*3*width + x*3 + 1];
				imageMatrix[2][y][x] = rawData[y*3*width + x*3 + 2];
			} else {
				// If it is still considered to be binary, and we only have
				// one value, then :
				if (isBinary && rawData[y*width+x] != v1 && v2 == -1) {
					v2 = (int) rawData[y*width+x];
				}
				// If it is still considered to be binary, adn we have both
				// values filled in, then :
				if (isBinary && v2 != -1 && rawData[y*width+x] != v1 && rawData[y*width+x] != v2) {
					isBinary = false;
				}
				imageMatrix[0][y][x] = rawData[y*width + x];
			}
		}
	}
	// If it is still considered a binary image, make the change for later use.
	if (isBinary && imageType == utils::loadedType::GREYSCALE) {
	imageType = utils::loadedType::BINARY;
	}

#ifdef DEBUG
	fprintf(stderr, "Image::load() : Finished writing from rawData to vector<vector<vector<unsigned char>>>.\n");
#endif

	fclose(imageFile);

#ifdef DEBUG
	fprintf(stderr, "Image::load() : end of call\n\n");
#endif

	return *this;
}

Image& Image::load(const std::string pathName) {
	return this->load(pathName.c_str());
}

void Image::writeToFile(const char* pathName) {

#ifdef DEBUG
	fprintf(stderr, "\n\nImage::writeToFile() called\n");
#endif

	if (imageType == utils::loadedType::NONE) {
		printf("Image::writeToFile() : The function could not proceed since there was \n");
		printf("\tno data inside the Image object.\n");
		exit(EXIT_FAILURE);
	}

	int imgSize = width * height * nbPlanes;
	// int pixCount = width * height; // UNUSED FOR NOW
	// Attempt to read the image (image files
	// must be loaded as binary, hence the B) :
	FILE* imageFile = fopen(pathName, "wb");

	if (imageFile == NULL) {
		printf("Image::writeToFile() : The file %s could not be opened.\n\n", pathName);
		exit(EXIT_FAILURE);
	}

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() : Attemtpting to write a %ix%i file onto disk at location %s ...\n", width, height, pathName);
#endif

	// Write contents to file :
	if (imageType == utils::loadedType::COLOR) {
		fprintf(imageFile, "P6\n");
	} else if (imageType == utils::loadedType::GREYSCALE || imageType == utils::loadedType::BINARY) {
		fprintf(imageFile, "P5\n");
	}

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() : Succesfully wrote file type\n");
#endif

	fprintf(imageFile, "# Image was inpainted using a custom algorithm.\n");
	fprintf(imageFile, "# Find more info at : \n");
	fprintf(imageFile, "# https://www.gitlab.com/fieloxx/inpainting/\n");
	// If the image is binary, send the info to the image as a comment
	// for the user that will pick this up later.
	if (imageType == utils::loadedType::BINARY) {
		fprintf(imageFile, "# Warning : this image was written as a greyscale\n# picture, but is in fact binary.\n");
	}

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() : Succesfully wrote comments\n");
#endif

	fprintf(imageFile, "%d %d\n%d\n", width, height, maxGreyValue);

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() : Successfully wrote image size\n");
#endif

	// convert the vector back into an array of unsigned char :
	unsigned char* rawData = new unsigned char[imgSize];

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() : Starting vector transformation procedure ...\n");
#endif

	for (unsigned int y = 0; y < this->imageMatrix[0].size(); ++y) {
		for (unsigned int x = 0; x < this->imageMatrix[0][y].size(); ++x) {
			for (unsigned int w = 0; w < nbPlanes; ++w) {
				rawData[y*nbPlanes*width + nbPlanes*x + w] = imageMatrix[w][y][x];
			}
		}
	}

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() : Ended vector transformation.\n");
	fprintf(stderr, "Image::writeToFile() : Starting writing process for data.\n");
#endif

	if (fwrite(rawData, sizeof(unsigned char), imgSize, imageFile) != (size_t)imgSize) {
	printf("Image::writeToFile() : Error whilst writing to image.\n");
	exit(EXIT_FAILURE);
	}

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() : Ended writing process for data.\n");
#endif

	fclose(imageFile);

#ifdef DEBUG
	fprintf(stderr, "Image::writeToFile() end of call.\n\n");
#endif

	return;
}

void Image::writeToFile(const std::string pathName) {
	this->writeToFile(pathName.c_str());
	return;
}

// }}}

// {{{ Info about the image

void Image::printInfo() const noexcept {
	switch (imageType) {
		case utils::loadedType::NONE:
			std::cout << "There is no image loaded in this object." << std::endl;
			break;;
		case utils::loadedType::BINARY:
			std::cout << "Currently loaded image is a binary image." << std::endl;
			break;
		case utils::loadedType::GREYSCALE:
			std::cout << "Currently loaded image is a greyscale image." << std::endl;
			break;
		case utils::loadedType::COLOR:
			std::cout << "Currently loaded image is a color image." << std::endl;
			break;
	}
	std::cout << "Its size is " << this->width << "x" << this->height << " on " << this->nbPlanes << " planes." << std::endl;
	std::cout << "The image matrix contains thus :" << std::endl;

	unsigned int p, y, x = 0;
	for (p = 0; p < this->imageMatrix.size(); ++p) {
		for (y = 0; y < this->imageMatrix[p].size(); ++y) {
			x += this->imageMatrix[p][y].size();
		}
	}
	std::cout << p << " planes; " << y << " lines for a total of " << x << " pixels." << std::endl;

	std::cout << "Finished." << std::endl;
}

// }}}

// {{{ Operators

Image& Image::operator= (const char* pathName) {
	this->imageMatrix.clear();

	this->load(pathName);

	return *this;
}

Image& Image::operator= (const std::string pathName) {
	this->imageMatrix.clear();

	this->load(pathName.c_str());

	return *this;
}

bool Image::operator==(const Image& i) const {
	if (this->imageType == i.imageType) {
		if (this->nbPlanes == i.nbPlanes) {
			if (this->maxGreyValue == i.maxGreyValue) {
				if (this->width == i.width) {
					if (this->height == i.height) {
					// Finally check image contents.

					#ifdef DEBUG
					fprintf(stderr, "\n\nImage::operator(const Image&) const : Strating to check pixel values, may SEGFAULT\n");
					#endif

					for (unsigned int p = 0; p < this->nbPlanes; ++p) {
						for (unsigned int y = 0; y < this->height; ++y) {
							for (unsigned int x = 0; x < this->width; ++x) {
								if (this->imageMatrix[p][y][x] != i.imageMatrix[p][y][x]) {
									return false;
								}
							}
						}
					}

					#ifdef DEBUG
					fprintf(stderr, "Image::operator(const Image&) const : Finished deep check.\n\n");
					#endif

					return true;
					}
				}
			}
		}
	}
	return false;
}

std::vector<std::vector<unsigned char>>& Image::operator[](const int p) {
	return this->imageMatrix[p];
}

const std::vector<std::vector<unsigned char>>& Image::operator[](const int p) const{
	return this->imageMatrix[p];
}

// }}}

// Domain changes (RGB, YCbCr, greyscale) {{{
Image& Image::toGreyscale() {
	#ifdef DEBUG
	fprintf(stderr, "\n\nImage::toGreyscale() called\n");
	fprintf(stderr, "Image::toGreyscale() : backing up image matrix.\n");
	#endif

	Image* copyCurrent = new Image(*this);

	if (copyCurrent->imageType == utils::loadedType::NONE) {
		printf("Image::toGreyscale() : could not convert image to greyscale, for none was loaded.\n");
		exit(EXIT_FAILURE);
	}

	if (copyCurrent->imageType == utils::loadedType::GREYSCALE) {
		printf("Image::toGreyscale() : WARNING : image was already greyscale.\n");
		return *copyCurrent;
	}

	if (copyCurrent->imageType == utils::loadedType::BINARY) {
		printf("Image::toGreyscale() : WARNING : image was binary.\n");
		return *copyCurrent;
	}

	std::vector<std::vector<std::vector<unsigned char>>> backupMatrix(this->imageMatrix);

	#ifdef DEBUG
	fprintf(stderr, "Image::toGreyscale() : backed up imageMatrix.\n");
	#endif

	copyCurrent->imageMatrix.clear();
	copyCurrent->imageMatrix.resize(1);
	copyCurrent->imageMatrix[0].resize(height);
	for (unsigned int i = 0; i < height; ++i) {
		copyCurrent->imageMatrix[0][i].resize(width);
	}

	#ifdef DEBUG
	fprintf(stderr, "Image::toGreyscale() : Cleared vector and resized it.\n");
	#endif

	copyCurrent->nbPlanes = 1;
	copyCurrent->imageType = utils::loadedType::GREYSCALE;

	#ifdef DEBUG
	fprintf(stderr, "Image::toGreyscale() : changed image class data.\n");
	#endif

	float coefR = 0.2126;
	float coefG = 0.7152;
	float coefB = 0.0722;

	float valR,valG,valB,valGrey;

	#ifdef DEBUG
	fprintf(stderr, "Image::toGreyscale() : starting image conversion.\n");
	#endif

	for (unsigned int y = 0; y < height; ++y) {
		for (unsigned int x = 0; x < width; ++x) {
			valR = (float) backupMatrix[0][y][x];
			valG = (float) backupMatrix[1][y][x];
			valB = (float) backupMatrix[2][y][x];

			valGrey = coefR * valR + coefG * valG + coefB * valB;

			copyCurrent->imageMatrix[0][y][x] = (unsigned char) std::roundf(valGrey);
		}
	}

	#ifdef DEBUG
	fprintf(stderr, "Image::toGreyscale() : finished greyscale conversion.\n");
	fprintf(stderr, "Image::toGreyscale() : call ended.\n\n");
	#endif

	return *copyCurrent;
}

Image& Image::toYCrCb() {
	if (this->imageType == utils::loadedType::COLOR) {
		for (size_t i = 0; i < this->height; ++i) {
			for (size_t j = 0; j < this->width; ++j) {
				double r = (double) this->imageMatrix[0][i][j];
				double g = (double) this->imageMatrix[1][i][j];
				double b = (double) this->imageMatrix[2][i][j];
				double cy = 016.0 + (65.738*r)/(256.0) + (129.057*g)/(256.0) + (25.064*b)/(256.0);
				double cb = 128.0 - (37.945*r)/(256.0) - (74.494*g)/(256.0) + (112.439*b)/(256.0);
				double cr = 128.0 + (112.439*r)/(256.0) - (94.154*g)/(256.0) - (18.285*b)/(256.0);
				this->imageMatrix[0][i][j] = (unsigned char) std::round(cy);
				this->imageMatrix[1][i][j] = (unsigned char) std::round(cb);
				this->imageMatrix[2][i][j] = (unsigned char) std::round(cr);
			}
		}
		return *this;
	} else {
		std::cout << "Image not in color, no modifications were done." << std::endl;
		return *this;
	}
}

Image& Image::toRGB() {
	if (this->imageType == utils::loadedType::COLOR) {
		for (size_t i = 0; i < this->height; ++i) {
			for (size_t j = 0; j < this->width; ++j) {
				double cy = (double) this->imageMatrix[0][i][j];
				double cb = (double) this->imageMatrix[1][i][j];
				double cr = (double) this->imageMatrix[2][i][j];
				double r = (298.082*cy)/(256.0) +                        (408.583*cr)/(256.0) - 222.921;
				double g = (298.082*cy)/(256.0) - (100.291*cb)/(256.0) - (208.120*cr)/(256.0) + 135.576;
				double b = (298.082*cy)/(256.0) + (516.412*cb)/(256.0)                        - 276.836;
				this->imageMatrix[0][i][j] = (unsigned char) std::min(std::max(std::round(r), .0), 255.0);
				this->imageMatrix[1][i][j] = (unsigned char) std::min(std::max(std::round(g), .0), 255.0);
				this->imageMatrix[2][i][j] = (unsigned char) std::min(std::max(std::round(b), .0), 255.0);
			}
		}
		return *this;
	} else {
		std::cout << "Image not in color, no modifications were done." << std::endl;
		return *this;
	}
}
// }}}

// Squeeze and Expand for compression {{{
Image& Image::squeezeAndExpand(size_t c1, size_t c2) {
	// c1 and c2 are the two components to squeeze/expand
	std::vector<std::vector<unsigned char>> nc1;
	std::vector<std::vector<unsigned char>> nc2;
	nc1.resize(this->height/2);
	nc2.resize(this->height/2);
	for (size_t i = 0; i < this->height; i+=2) {
		nc1[i/2].resize(this->width/2 + this->width%2);
		nc2[i/2].resize(this->width/2 + this->width%2);
		for (size_t j = 0; j < this->width; j+=2) {
			double mean = .0;
			// do it for component 1 :
			{
				/* lu = left-up */
				double lu = (double) this->imageMatrix[c1][i+0][j+0];
				/* ru = right-up */
				double ru = (double) this->imageMatrix[c1][i+0][j+1];
				/* ld = left-down */
				double ld = (double) this->imageMatrix[c1][i+1][j+0];
				/* rd = right-down */
				double rd = (double) this->imageMatrix[c1][i+1][j+1];
				mean = (lu + ru + ld + rd) / 4.0;
				nc1[i/2][j/2] = (unsigned char) std::round(mean);
			}
			// do it for component 2 :
			{
				/* lu = left-up */
				double lu = (double) this->imageMatrix[c2][i+0][j+0];
				/* ru = right-up */
				double ru = (double) this->imageMatrix[c2][i+0][j+1];
				/* ld = left-down */
				double ld = (double) this->imageMatrix[c2][i+1][j+0];
				/* rd = right-down */
				double rd = (double) this->imageMatrix[c2][i+1][j+1];
				mean = (lu + ru + ld + rd) / 4.0;
				nc2[i/2][j/2] = (unsigned char) std::round(mean);
			}
		}
	}
	for (size_t i = 0; i < nc1.size(); ++i) {
		for (size_t j = 0; j < nc1[i].size(); ++j) {
			this->imageMatrix[c1][2*i+0][2*j+0] = nc1[i][j];
			this->imageMatrix[c1][2*i+0][2*j+1] = nc1[i][j];
			this->imageMatrix[c1][2*i+1][2*j+0] = nc1[i][j];
			this->imageMatrix[c1][2*i+1][2*j+1] = nc1[i][j];
			this->imageMatrix[c2][2*i+0][2*j+0] = nc2[i][j];
			this->imageMatrix[c2][2*i+0][2*j+1] = nc2[i][j];
			this->imageMatrix[c2][2*i+1][2*j+0] = nc2[i][j];
			this->imageMatrix[c2][2*i+1][2*j+1] = nc2[i][j];
		}
	}
	return *this;
}

Image& Image::squeezeAndExpandInYCrCb() {
	this->toYCrCb();
	std::vector<std::vector<unsigned char>> nc1;
	std::vector<std::vector<unsigned char>> nc2;
	nc1.resize(this->height/2);
	nc2.resize(this->height/2);
	for (size_t i = 0; i < this->height; i+=2) {
		nc1[i/2].resize(this->width/2 + this->width%2);
		nc2[i/2].resize(this->width/2 + this->width%2);
		for (size_t j = 0; j < this->width; j+=2) {
			double mean = .0;
			// do it for component 1 :
			{
				/* lu = left-up */
				double lu = (double) this->imageMatrix[1][i+0][j+0];
				/* ru = right-up */
				double ru = (double) this->imageMatrix[1][i+0][j+1];
				/* ld = left-down */
				double ld = (double) this->imageMatrix[1][i+1][j+0];
				/* rd = right-down */
				double rd = (double) this->imageMatrix[1][i+1][j+1];
				mean = (lu + ru + ld + rd) / 4.0;
				nc1[i/2][j/2] = (unsigned char) std::round(mean);
			}
			// do it for component 2 :
			{
				/* lu = left-up */
				double lu = (double) this->imageMatrix[2][i+0][j+0];
				/* ru = right-up */
				double ru = (double) this->imageMatrix[2][i+0][j+1];
				/* ld = left-down */
				double ld = (double) this->imageMatrix[2][i+1][j+0];
				/* rd = right-down */
				double rd = (double) this->imageMatrix[2][i+1][j+1];
				mean = (lu + ru + ld + rd) / 4.0;
				nc2[i/2][j/2] = (unsigned char) std::round(mean);
			}
		}
	}
	for (size_t i = 0; i < nc1.size(); ++i) {
		for (size_t j = 0; j < nc1[i].size(); ++j) {
			this->imageMatrix[1][2*i+0][2*j+0] = nc1[i][j];
			this->imageMatrix[1][2*i+0][2*j+1] = nc1[i][j];
			this->imageMatrix[1][2*i+1][2*j+0] = nc1[i][j];
			this->imageMatrix[1][2*i+1][2*j+1] = nc1[i][j];
			this->imageMatrix[2][2*i+0][2*j+0] = nc2[i][j];
			this->imageMatrix[2][2*i+0][2*j+1] = nc2[i][j];
			this->imageMatrix[2][2*i+1][2*j+0] = nc2[i][j];
			this->imageMatrix[2][2*i+1][2*j+1] = nc2[i][j];
		}
	}
	this->toRGB();
	return *this;
}
// }}}

/* TP1_ChiffrementMultimedia {{{ */

uint8_t Image::encrypt(unsigned int key) {
	if (this->imageType != utils::loadedType::GREYSCALE) {
		std::cerr << "Could not encrypt, not greyscale" << std::endl;
		return static_cast<uint8_t>(0);
	}
	if (key > 100) {
		std::cerr << "Could not encrypt : key too big for this exercise" << std::endl;
		return static_cast<uint8_t>(0);
	}
	srand(key);
	for (size_t i = 0; i < this->height; ++i) {
		for (size_t j = 0; j < this->width; ++j) {
			uint8_t random_key = rand() % 255;
			this->imageMatrix[0][i][j] = this->imageMatrix[0][i][j] ^ random_key;
		}
	}
	return static_cast<uint8_t>(0);
}

void Image::decrypt(uint8_t key) {
	if (this->imageType != utils::loadedType::GREYSCALE) {
		std::cerr << "Could not encrypt, not greyscale" << std::endl;
		return;
	}

	srand(key);

	for (size_t i = 0; i < this->height; ++i) {
		for (size_t j = 0; j < this->width; ++j) {
			uint8_t random_key = rand() % 255;
			this->imageMatrix[0][i][j] = this->imageMatrix[0][i][j] ^ random_key;
		}
	}
}

void Image::decrypt_brute_force() {
	std::vector<double> entropies(101, .0);

	for (size_t i = 0; i <= 100; ++i) {
		this->decrypt(i);
		entropies[i] = this->compute_entropy();
		this->encrypt(i);
	}

	double min = 8.01;
	size_t min_idx = 0;
	for (size_t i = 0; i < entropies.size(); ++i) {
		if (entropies[i] < min) {
			min = entropies[i];
			min_idx = i;
		}
	}
	std::cout << "Key was " << min_idx << std::endl;
	this->decrypt(static_cast<uint8_t>(min_idx));
}

double Image::compute_entropy() const {
	std::vector<uint32_t> occurences(256, 0);
	std::vector<double> probabilities;

	for (size_t i = 0; i < this->height; ++i) {
		for (size_t j = 0; j < this->width; ++j) {
			occurences[this->imageMatrix[0][i][j]]++;
		}
	}

	for (const uint32_t& occ : occurences) {
		probabilities.push_back(static_cast<double>(occ) / static_cast<double>(this->width * this->height));
	}

	std::cerr << "computed probabilities" << std::endl;

	double entropy = .0;
	for (const double& proba : probabilities) {
		if (proba > .0) {
			entropy += proba * std::log2(proba);
		}
	}
	entropy = -(entropy);

	std::cerr << "Computed entropy at : " << entropy << std::endl;

	return entropy;
}

/* end of TP1_ChiffrementMultimedia }}} */

// TP2_ChiffrementMultimedia {{{
void Image::very_simple_rsa(unsigned int p, unsigned int q, unsigned int e) {
	for (unsigned int i = 0; i < this->imageMatrix[0].size(); ++i) {
		for (unsigned int j = 0; j < this->imageMatrix[0][i].size(); ++j) {
			this->imageMatrix[0][i][j] = rsa_modulo(this->imageMatrix[0][i][j], p*q, e);
		}
	}
}

void Image::very_simple_reverse_rsa(unsigned int phi, unsigned int n, unsigned int e) {
	for (unsigned int i = 0; i < this->imageMatrix[0].size(); ++i) {
		for (unsigned int j = 0; j < this->imageMatrix[0][i].size(); ++j) {
			this->imageMatrix[0][i][j] = rsa_modulo(this->imageMatrix[0][i][j], n, inverse_modulo(e, phi));
		}
	}
}
// }}}

void Image::compress_with_wavelets(size_t max_iter, double quant_default) {
	std::cout << "Image size currently standing at " << this->getSize() * sizeof(unsigned char) * 8 << " bits." << std::endl;
	std::cout << "Before compression, image could be compressed down to " << this->compute_huffman_expected_size() << " bits using Huffman." << std::endl;
	this->toYCrCb();

	for (size_t i = 0; i < this->nbPlanes; ++i) {
		matrix m(*this, i);
		m.path_name_for_debug = "haar_" + std::to_string(i) + "_";
		m.quantification = quant_default;
		m.max_haar = max_iter;
		m.haar_transform();
		m.inverse_haar();
		m.copy_to_image(*this, i);
	}

	this->toRGB();
	std::cout << "After compression, image could be compressed down to " << std::fixed << this->compute_huffman_expected_size() << " bits using Huffman." << std::endl;
	std::cout << "Gains would be of about "
		<< (1.0 - static_cast<double>(this->compute_huffman_expected_size()) / static_cast<double>(this->getSize() * sizeof(unsigned char) * 8)) * 100.0 
		<< "%." << std::endl;
}

size_t Image::compute_huffman_expected_size() {
	size_t total_huffman_size = 0;

	image_huffman_generator current_huffman(*this, this->nbPlanes+1);

	return current_huffman.compute_huffman_tree_generation();
}

void Image::applyKMeans(unsigned int nbClusters, unsigned int paletteWidth) {
	double acceptDelta = 0.05;
	unsigned int maxIter = 1000;

	imageKMeanClusterer kMeansClusterer = imageKMeanClusterer(this->imageMatrix, nbClusters, maxIter, acceptDelta);

	this->imageMatrix = kMeansClusterer.compute(paletteWidth);
	return; // Image(*this);
}


double Image::PSNR(const Image& _i) const {
	double mean = 0.0;
	unsigned long size = this->height * this->width;
	for (unsigned int i = 0; i < this->height && i < _i.getHeight(); ++i) {
		for (unsigned int j = 0; j < this->width && i < _i.getWidth(); ++j) {
			std::vector<unsigned char> current_pixel = std::vector<unsigned char>();
			std::vector<unsigned char> old_pixel = std::vector<unsigned char>();
			for (size_t c = 0 ; c < this->nbPlanes ; ++c) {
				current_pixel.push_back(this->imageMatrix[c][i][j]);
				old_pixel.push_back(_i[c][i][j]);
			}

			assert(old_pixel.size() == current_pixel.size());

			double length = .0;
			for (size_t k = 0; k < old_pixel.size(); ++k) {
				unsigned char c1 = current_pixel[k];
				unsigned char c2 = old_pixel[k];
				if (c1 > c2) {
					length += std::pow(static_cast<double>(c1 - c2), 2.0);
				} else {
					length += std::pow(static_cast<double>(c2 - c1), 2.0);
				}
			}
			mean += length;
		}
	}
	mean /= (double)size;
	return 10.0 * std::log10(255.0*255.0 / mean);
}

bool is_prime(unsigned int k) {
	if (k == 1 || k == 0) return false;
	if (k % 2 == 0 && k > 2) return false;
	if (k % 3 == 0 && k > 3) return false;
	if (k == 2 || k == 3) return true;

	unsigned int high_limit = k / 2;
	for (unsigned int i = 2; i < high_limit; ++i) {
		if (k % i == 0) {
			return false;
		}
	}
	return true;
}

bool are_prime(unsigned int k, unsigned int l) {
	if (k == 0 || l == 0) return false;
	return (std::__gcd(k,l) == 1);
}

std::vector<unsigned int> get_all_exponents_for(unsigned int e) {
	std::vector<uint> exps;
	for (unsigned int i = e; i > 1; --i) {
		if (are_prime(e, i)) {
			exps.push_back(i);
		}
	}
	return exps;
}

unsigned char rsa_modulo(unsigned char in, unsigned int n, unsigned int e) {
	unsigned int val = static_cast<unsigned int>(in);
	unsigned int base = static_cast<unsigned int>(in);

	while (e > 1) {
		( val *= base ) %= n;
		e--;
	}

	return static_cast<unsigned char>(val);
}

unsigned int inverse_modulo(unsigned int e, unsigned int phi_n) {
	for(unsigned int i = phi_n-1; i > 0; --i) {
		if ((e * i) % phi_n == 1) return i;
	}
	std::cout << "!";
	return 0;
}

void Image::writeHistogramTo(const char* pathName) const {

#ifdef DEBUG
	fprintf(stderr, "\n\nImage::writeHistogramTo(const char*) : Opening file %s ...\n", pathName);
#endif

	FILE* histoFile = fopen(pathName, "w");

	if (histoFile == NULL) {
		printf("Image::writeHistogramTo(const char*) : \nCould not read file %s !\n", pathName);
		exit(EXIT_FAILURE);
	}

#ifdef DEBUG
	fprintf(stderr, "Image::writeHistogramTo(const char*) : Fetching values using Image::histogram() : \n");
#endif

	std::vector<std::vector<unsigned char>> values = histogram();

#ifdef DEBUG
	fprintf(stderr, "Image::writeHistogramTo(const char*) : Fetched values.\n");
	fprintf(stderr, "Image::writeHistogramTo(const char*) : Strating writing process to file %s ...", pathName);
#endif

	for (unsigned int i = 0; i < values.size(); ++i) {
		for (unsigned int p = 0; p < values[p].size(); ++p) {
			fprintf(histoFile, "%d ", values[i][p]);
		}
		fprintf(histoFile, "\n");
	}

#ifdef DEBUG
	fprintf(stderr, "Image::writeHistogramTo(const char*) : Wrote values to file %s.\n", pathName);
#endif

	fclose(histoFile);

#ifdef DEBUG
	fprintf(stderr, "Image::writeToHistogram(const char*) : end call.\n\n");
#endif

	return;
}
std::vector<std::vector<unsigned char>> Image::histogram() const {

#ifdef DEBUG
	fprintf(stderr, "\n\nImage::histogram() called\n");
	fprintf(stderr, "Image::histogram() : Allocating vector for histogram values\n");
#endif

	if (imageType == utils::loadedType::NONE) {
		printf("Image::histogram() : \nCould not make a histogram of the image, since none was loaded in memory.\n");
		exit(EXIT_FAILURE);
	}

	std::vector<std::vector<unsigned char>> histoValues;
	std::cout << "========================" << this->maxGreyValue << std::endl;;
	histoValues.resize(nbPlanes);
	for (unsigned int i = 0; i < nbPlanes; ++i) {
		histoValues[i].resize(maxGreyValue);
		for (unsigned int j = 0; j < histoValues[i].size(); ++j) {
			histoValues[i][j] = 0;
		}
	}

#ifdef DEBUG
	fprintf(stderr, "Image::histogram() : Allocated space for histogram values.\n");
	fprintf(stderr, "Image::histogram() : Strating to read image to fill histogram\n");
#endif

	for (unsigned int p = 0; p < nbPlanes; ++p) {
		for (unsigned int y = 0; y < height; ++y) {
			for (unsigned int x = 0; x < width; ++x) {
				histoValues[p][ imageMatrix[p][y][x] ]++;
			}
		}
	}

#ifdef DEBUG
	fprintf(stderr, "Image::histogram() : histogram created.\n\n");
#endif

	return histoValues;
}
Image& Image::threshold(unsigned char t) {
	/**
	* 1. if the image is in color, first we need to make it greyscale,
	* and then ...
	*
	* 2: iterate through the matrix, and if the pixel is above threshold
	* then set it to 255, otherwise set it to 0.
	*/

#ifdef DEBUG
	fprintf(stderr, "\n\nImage::threshold(%u) called.\n", t);
	fprintf(stderr, "Copying current image into a copy ...\n", t);
#endif

	Image cc;

	if (this->imageType == utils::loadedType::COLOR) {
		cc = this->toGreyscale();
	} else {
		cc = Image(*this);
	}

	Image* copyCurrent = new Image(cc);

#ifdef DEBUG
	fprintf(stderr, "Image::threshold() : Copied image.\n");
#endif

#ifdef DEBUG
	fprintf(stderr, "Image::threshold() : Image is now greyscale\n");
	fprintf(stderr, "Image::threshold() : Starting to apply threshold ...\n");
#endif

	for (unsigned int i = 0; i < copyCurrent->imageMatrix[0].size(); ++i) {
		for (unsigned int j = 0; j < copyCurrent->imageMatrix[0][i].size(); ++j) {
			copyCurrent->imageMatrix[0][i][j] = (copyCurrent->imageMatrix[0][i][j] >= t) ? (unsigned char)255 : (unsigned char)0;
		}
	}

	copyCurrent->imageType = utils::loadedType::BINARY;

#ifdef DEBUG
	fprintf(stderr, "Image::threshold() : Finished applying threshold and changed imageType.\n");
	fprintf(stderr, "Image::threshold() : end call. Returning a new object.\n\n");
#endif

	return *copyCurrent;
}

