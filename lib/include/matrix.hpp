#ifndef _LIB_INCLUDE_MATRIX_
#define _LIB_INCLUDE_MATRIX_

#include "image.hpp"
#include <iomanip>

//#define __MATRIX_DEBUG_ON
//#define __MATRIX_DEBUG_LEVEL_BASIC
#define __MATRIX_APPLY_QUANTIFICATION

class matrix {
	private:
		/* raw data for transform */
		std::vector<std::vector<double>> raw_data;
		/* max haar iteration reached */
		size_t max_haar_reached;
		/* copy image lol */
		Image& original_image;
	public:
		/* yo the fuck */
		std::string path_name_for_debug;
		/* yo the hell */
		double quantification;
		/* max haar iteration possible */
		size_t max_haar;
	public:
		/* default constructor */
		/* constructor with an image */
		matrix(Image&, size_t);
		/* apply haar transform */
		void haar_transform();
		/* apply inverse haar transform on an image */
		void inverse_haar();
		/* copy data to image */
		void copy_to_image(Image&, size_t) const;
		/* operator for printing */
		friend std::ostream& operator<<(std::ostream& os, const matrix& m) {
			os << "matrix of " << m.raw_data.size() << " rows" << ((m.raw_data.size() == 0) ? "." : " :") << std::endl;
			for (size_t i = 0; i < m.raw_data.size(); ++i) {
				os << std::setw(4) ;
				for (double d : m.raw_data[i]) {
					os << ((d < 0) ? std::setw(5) : std::setw(6)) << d << " // ";
				}
				os << std::endl;
			}
			return os;
		}
	private:
		/* apply Nth step of haar transform */
		void haar_transform(size_t, const std::string&);
		/* apply Nth step of inverse haar transform */
		void inverse_haar(size_t, const std::string&);
		/* copy current state to a new image */
		void output_current_state_to(const std::string&) const;
};

#endif // _LIB_INCLUDE_MATRIX_
