#ifndef _LIB_INCLUDE_HUFFMAN_
#define _LIB_INCLUDE_HUFFMAN_

#include "image.hpp"

#include <map>
#include <stack>
#include <cassert>

typedef enum {
	DATA = 0,
	NODE = 1,
	GENERIC
} huffman_tree_data_types;

typedef size_t freq_t; // doesn't count frequency, but number of occurences of a symbol

/* generic_node : Generic class for a node {{{ */
class generic_node {
	public:
		/* frequency of apparition withing the signal */
		freq_t f;
	public:
		/* base constructor with frequency */
		generic_node(const freq_t _f) : f(_f) {}
		/* const copy constructor */
		generic_node(const generic_node& gn) : f(gn.f) {}
		/* recursive printing of the tree */
		virtual void print_node(std::string = "") const {std::cout << "generic_node" << std::endl;}
		/* gets the type of the node */
		virtual huffman_tree_data_types get_node_type() const { return huffman_tree_data_types::GENERIC; };
		/* destructor */
		virtual ~generic_node() {}
};
// }}}

/* tree_node : A regular node in a Huffman tree {{{ */
class tree_node : public virtual generic_node {
	public:
		const generic_node *left;
		const generic_node *right;

		/* constructor */
		explicit tree_node(generic_node* l, generic_node* r) : generic_node(l->f + r->f), left(l), right(r) {}
		tree_node(const tree_node& tn) : generic_node(tn.f), left(tn.left), right(tn.right) {}
		virtual void print_node(std::string = "") const override;
		virtual huffman_tree_data_types get_node_type() const override { return huffman_tree_data_types::NODE; }
		virtual ~tree_node() override { delete left; delete right; }
};
// }}}

/* data_node : A leaf node in the Huffman tree, containing data {{{ */
template <typename data_class> class data_node : public virtual generic_node {
	public:
		data_class data;

		/* constructor */
		data_node(freq_t _f, data_class _data) : generic_node(_f), data(_data) {};
		data_node(const data_node& dn) : generic_node(dn.f), data(dn.data) {}
		virtual void print_node(std::string = "") const override;
		huffman_tree_data_types get_node_type() const override { return huffman_tree_data_types::DATA; }
		virtual ~data_node() override {}
};
// }}}

/* node_comparator : Struct for node comparison {{{ */
struct node_comparator {
	bool operator()(const generic_node* lhs, const generic_node* rhs) {
		return lhs->f > rhs->f;
	}
};
// }}}

/* Class to generate a Huffman tree from an image {{{ */
class image_huffman_generator {
	public:
		/* original image data */
		std::vector<std::vector<unsigned char>> image_data;
		/* original image reference */
		Image& ref_image;
		/* Root of the generated tree */
		generic_node* tree_root;

		/* constructor */
		image_huffman_generator(Image& i, size_t plane);
		/* compute huffman values for the current image
		and compute memory size for the current image */
		size_t compute_huffman_tree_generation();
};
/* }}} */

/* Huffman tree traversal for the current image, to get symbol sizes {{{ */
void image_huffman_tree_traversal(const generic_node*, std::stack<bool>&, std::map<unsigned char, size_t>&);
/* }}} */

#endif // _LIB_INCLUDE_HUFFMAN_
