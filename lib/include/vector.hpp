#ifndef _LIB_INCLUDE_VECTOR_HPP_
#define _LIB_INCLUDE_VECTOR_HPP_

#include <math.h> // for sqrt
#include <iostream>

template <typename T> class _rgb {

	// Public members {{{
	public:
		T r; /* Red component   */
		T g; /* Green component */
		T b; /* Blue component  */
	// }}}

	public:

		// Constructors : simple, 1D, 3D, and copy {{{
		/* Default constructor */
		_rgb() {
			this->r = (T)0;
			this->g = (T)0;
			this->b = (T)0;
		};
		/* Single-scalar constructor */
		_rgb(T s) {
			this->r = s;
			this->g = s;
			this->b = s;
		};
		/* Full _rgb constructor */
		_rgb(T _r, T _g, T _b) {
			this->r = _r;
			this->g = _g;
			this->b = _b;
		};
		/* Full _rgb constructor with a different type */
		template <typename U> _rgb(U _r, U _g, U _b) {
			this->r = (T)_r;
			this->g = (T)_g;
			this->b = (T)_b;
		};
		/* Copy constructor */
		_rgb(const _rgb<T>& __rgb) {
			this->r = __rgb.r;
			this->g = __rgb.g;
			this->b = __rgb.b;
		};
		/* Copy constructor from another type */
		template <typename U> _rgb(const _rgb<U>& __rgb) {
			this->r = (T)__rgb.r;
			this->g = (T)__rgb.g;
			this->b = (T)__rgb.b;
		};
		// }}}

		double length() const {
			return std::sqrt((double)this->r * (double)this->r +
					(double)this->g * (double)this->g +
					(double)this->b * (double)this->b);
		}

		_rgb normalize() const {
			double size = this->length();
			return _rgb<T>(this->r/size, this->g/size, this->b/size);
		}

		// Operators : addition, self, substraction, division, self, output {{{
		/* addition operator onto a new vector */
		_rgb  operator+ (const _rgb& __rgb) const {
			return _rgb<T>(this->r + __rgb.r,
					this->g + __rgb.g,
					this->b + __rgb.b);
		};
		/* self-addition operator */
		_rgb& operator+=(const _rgb<T>& __rgb) {
			this->r += __rgb.r;
			this->g += __rgb.g;
			this->b += __rgb.b;
			return *this;
		};
		/* substraction operator */
		_rgb  operator- (const _rgb<T>& __rgb) const {
			T distR;
			T distG;
			T distB;
			if (this->r > __rgb.r) {
				distR = this->r - __rgb.r;
			} else {
				distR = __rgb.r - this->r;
			}
			if (this->g > __rgb.g) {
				distG = this->g - __rgb.g;
			} else {
				distG = __rgb.g - this->g;
			}
			if (this->b > __rgb.b) {
				distB = this->b - __rgb.b;
			} else {
				distB = __rgb.b - this->b;
			}
			return _rgb<T>(distR, distG, distB);
		};
		/* Scalar division operator */
		_rgb  operator/ (const double s) const {
			// Casts the member into a double, performs
			// the division, and re-casts it into the
			// desired type T
			return _rgb<T>((T)((double)this->r / s) ,
					(T)((double)this->g / s) ,
					(T)((double)this->b / s) );
		};
		/* self-division operator */
		_rgb& operator/=(const double s) {
			this->r /= s;
			this->g /= s;
			this->b /= s;
			return *this;
		}
		/* casting type */
		template <typename U> operator _rgb<U>() const {
			return _rgb<U>(	(U)this->r,
					(U)this->g,
					(U)this->b);
		}
		/* comparison */
		bool operator==(const _rgb<T>& __rgb) const {
			return this->r == __rgb.r && this->g == __rgb.g && this->b == __rgb.b;
		}
		/* output operator */
		friend std::ostream& operator<<(std::ostream& os, const _rgb<T>& __rgb) {
			return os << "_rgb:(" << __rgb.r << "," << __rgb.g << "," << __rgb.b << ")";
		};
		// }}}

};

typedef _rgb<double>		rgb_d;
typedef _rgb<float>		rgb_f;
typedef _rgb<unsigned char>	rgb_uc;
typedef _rgb<unsigned int>	rgb_ui;
typedef _rgb<int>		rgb_i;
// Default rgb type :
typedef rgb_d			rgb;

#endif // _LIB_INCLUDE_VECTOR_HPP_
