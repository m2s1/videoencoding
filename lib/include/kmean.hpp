#ifndef _LIB_INCLUDE_KMEAN_HPP_
#define _LIB_INCLUDE_KMEAN_HPP_

#include "vector.hpp"
#include <vector>

class imageKMeanClusterer {
	public:
		std::vector<rgb> clusterCenters;	/* cluster centers (usually chosen randomly at the start) */
		std::vector<rgb> rawData;		/* raw RGB data to work with, will not be changed during */
		std::vector<unsigned int> labels;	/* labels for each point of to compute clusters */
		unsigned int maxIter;			/* nb of iterations allowed for convergeance */
		double minDelta;			/* delta to consider for convergeance reached */
		unsigned int height;
		unsigned int width;

	public:
		/* Default constructor */
		imageKMeanClusterer(const std::vector<std::vector<std::vector<unsigned char>>> imgData, unsigned int nbClusters, unsigned int nbIter, double delta);
		/* Compute method */
		std::vector<std::vector<std::vector<unsigned char>>> compute(unsigned int paletteWidth);
		/* Re-centering of cluster */
		void updateClusterCenters();
		/* label updater */
		void updateLabels();
		/* Print palette to a file */
		void printPalette(const char* pathName, unsigned int _r = 4) const;
		/* Prints info about clusters */
		void printClusterInfo() const;
};

#endif // _LIB_INCLUDE_KMEAN_HPP_
