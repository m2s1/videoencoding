/*******************************************
** FILE : image.hpp
** AUTH : Thibault de VILLÈLE
** DATE : 13/Sept/2019
** DESC : Classe image, et utilitaires pour
**        les lires/les écrire
*******************************************/

#ifndef _INCLUDE_IMAGE_HPP_
#define _INCLUDE_IMAGE_HPP_

#include <vector>	// std::vector
#include <iostream>	// Standard I/O
#include <fstream>	// File I/O

#include "vector.hpp"
#include "kmean.hpp"

//#define DEBUG

namespace utils {

	/**
	** Allows for easy checking of the kind of image loaded, if any.
	**/
	typedef enum {
		NONE,
		GREYSCALE,
		COLOR,
		BINARY
	} loadedType;

	void ignorerCommentaires(FILE* imageFile);

}

class Image {

	/* Private data : imageMatrix, width, height, nbPlanes, maxGreyValue {{{ */
	private:
		/* Represents the image, coded as an array of 2D matrices for which
		each one represents a 'plane' of the image. In the case of RGB
		images, each plane is a separate color between red, green, and blue.
		For greyscale images, the only plane available is the grey plane. */
		std::vector< std::vector< std::vector<unsigned char>>> imageMatrix;
		/* Allows the user to know which kind of image is loaded into the object, if any. */
		utils::loadedType imageType;
		/* Image width, measured in pixels. */
		unsigned int width;
		/* Image height, measured in pixels. */
		unsigned int height;
		/* Number of planes the image has. For greyscale images, this will be
		set to one, for RGB it will be set to 3, and for when an image has
		not yet been loaded in, it will be set to 0. */
		unsigned int nbPlanes;
		/* Represents the maximum value of any pixel of the image. Currently, this
		library only supports up to 255 greyscale/RGB values. Since the PGM/PPM
		file specification allows up to 65535 differents values for each pixel,
		it will not read any files with more than 255 values for a given pixel. */
		unsigned int maxGreyValue;
	/* }}} */

	public:
		/* Constructors {{{ */
		/* Default constructor. Doesn't do anything, other than to set the
		variable `imageType` to `NONE` */
		Image();

		/* Image constructor that also takes into account a filename given. */
		Image(const char* pathName);

		/* Image copy constructor. */
		Image(const Image& i);

		/* Constructor from a stream of pixels and an old image : */
		Image(const Image& i, const std::vector<rgb_uc>& pixels);
		/* }}} */

		/* Accessors for private data {{{ */
		/* Returns the width of an image. */
		const unsigned int getWidth() const noexcept;

		/* Get the height of an image. */
		const unsigned int getHeight() const noexcept;

		/* Get the number of pixels (width x height) of an image. */
		const unsigned int getPixelCount() const noexcept;

		/* Get the number of data values (width x height x nbPlanes) of an image. */
		const unsigned int getSize() const noexcept;

		/* Get the image type of the loaded image, if any. */
		const utils::loadedType getImageType() const noexcept;
		/* }}} */

		// Setters {{{

		/* Sets the size of the image, in Height x Width x Depth (number of planes) */
		void setSize(size_t, size_t, size_t);

		/* Sets the image's matrix for a pixel (in operator form) */
		void operator()(size_t,size_t,size_t,unsigned char);

		// }}}

	/* TP1_ChiffrementMultimedia {{{ */

		/* encrypt using XOR method */
		uint8_t encrypt(unsigned int);

		/* encrypt using XOR method */
		void decrypt(uint8_t);

		/* decrypt using brute force */
		void decrypt_brute_force();

		/* compute entropy */
		double compute_entropy() const;

	/* }}} */

	/* TP2_ChiffrementMultimedia {{{ */
		void very_simple_rsa(unsigned int p, unsigned int a, unsigned int e);
		void very_simple_reverse_rsa(unsigned int phi, unsigned int n, unsigned int e);
	/* }}} */

		/* compress an image with wavelets */
		void compress_with_wavelets(size_t = 6, double = 1.0);

		/* compute expected size if compressed using huffman encoding */
		size_t compute_huffman_expected_size();

		/* Prints the information about the currently loaded image. */
		void printInfo() const noexcept;

		/* Allows for the user to read a file into memory. */
		Image& load(const char* pathName);

		/* Allows for the user to read a file into memory. */
		Image& load(const std::string pathName);

		/* Allows for the user to write the image onto a file on their system. */
		void writeToFile(const char* pathName);

		/* Allows for the user to write the image onto a file on their system. */
		void writeToFile(const std::string pathName);

		/* Creates an histogram of the image, and returns it. */
		std::vector<std::vector<unsigned char>> histogram() const;

		/* Converts a color image to greyscale. */
		Image& toGreyscale();

		/* threshold */
		Image& threshold(unsigned char t);

		/* Converts an RGB image to YCrCb */
		Image& toYCrCb();

		/* Converts an RGB image to YCrCb */
		Image& toRGB();

		/* Squeeze and expand two components of the image */
		Image& squeezeAndExpand(size_t, size_t);

		/* Squeeze and expand two components of the image */
		Image& squeezeAndExpandInYCrCb();

		/* Allows the user to do a kMeans segmentation on the image */
		void applyKMeans(unsigned int nbClusters, unsigned int paletteWidth = 4);

		/* Compute PSNR for a given image compared to this one */
		double PSNR(const Image& _i) const;

		////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////          OPERATORS         ////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////

		/* Allows to load an image easily in memory. */
		Image& operator= (const char* pathName);

		/* Allows to load an image easily in memory. */
		Image& operator= (const std::string pathName);

		/* Allows for the user to check if two images are the same. */
		bool operator==(const Image& i) const;

		/* Allows to access the pixel values inside the image ! */
		std::vector<std::vector<unsigned char>>& operator[](const int);

		/* Same as above, but non-mutable */
		const std::vector<std::vector<unsigned char>>& operator[](const int) const;

		// write hitso
		void writeHistogramTo(const char* pathName) const;
};

bool is_prime(unsigned int);
bool are_prime(unsigned int, unsigned int);
std::vector<unsigned int> get_all_exponents_for(unsigned int);
unsigned char rsa_modulo(unsigned char, unsigned int, unsigned int);
unsigned int inverse_modulo(uint, uint);

#endif // _INCLUDE_IMAGE_HPP_
