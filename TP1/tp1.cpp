#include "../lib/include/image.hpp"
#include "../lib/include/kmean.hpp"

int main(int argc, char* argv[]) {
	Image loadedImage = Image("../img/parrot.ppm");
	Image comparison(loadedImage);
	loadedImage.applyKMeans(256, 16);
	loadedImage.writeToFile("./imgKMEANS.ppm");
	std::cout << "PSNR of the image : " << loadedImage.PSNR(comparison);
	return 0;
}
